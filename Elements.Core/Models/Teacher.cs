﻿using System;
using System.Collections.Generic;
using System.Text;
using Elements.Core.Interfaces;


namespace Elements.Core.Models
{
    public class Teacher: IIdentityUser
    {
        public Teacher()
        {
            Classrooms = new List<TeacherClassroom>();
        }
        public string Name { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public string ApplicationUserId { get; set; }
        public int SchoolId { get; set; }
        public ICollection<TeacherClassroom> Classrooms { get; set; }
    }
}
