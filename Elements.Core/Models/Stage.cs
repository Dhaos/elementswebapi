﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Elements.Core.Models
{
    public class Stage
    {   public int Id { get; set; }
        public string Name { get; set; }
        public int LessonId { get; set; }
        public Lesson Lesson { get; set; }
        public int OrderInLesson { get; set; }
        public int TotalVinyls { get; set; }

        public ICollection<Exercise> Exercises { get; set; }

    }
}
