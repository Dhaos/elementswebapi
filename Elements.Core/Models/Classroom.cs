﻿using System.Collections.Generic;

namespace Elements.Core.Models
{
    public class Classroom
    {
        public Classroom()
        {
            Teachers = new List<TeacherClassroom>();
            Students = new List<StudentClassroom>();
        }
        public string Name { get; set; }
        public int Id { get; set; }
        public ICollection<TeacherClassroom> Teachers { get; set; }
        public ICollection<StudentClassroom> Students { get; set; }
        public string Code { get; set; }
        public School School { get; set; }
        public int SchoolId { get; set; }

        public int MaxStudents { get; set; }
    }
}