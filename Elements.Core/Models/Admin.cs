﻿using System;
using Elements.Core.Interfaces;

namespace Elements.Core.Models
{
    public class Admin : IIdentityUser
    {
        public int Id { get; set; }
        public string ApplicationUserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int SchoolId { get; set; }
        public School School { get; set; }

        public bool IsSuper { get; set; }
    }
}
