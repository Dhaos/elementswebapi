﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Elements.Core.Models
{
    public class BlockedStudent
    {
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public School School { get; set; }
        public int SchoolId { get; set; }
    }
}
