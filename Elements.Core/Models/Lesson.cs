﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Elements.Core.Models
{
    public class Lesson
    {   public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Stage> Stages { get; set; }
        public int LevelId { get; set; }
        public int OrderInLevel { get; set; }
        public Level Level { get; set; }
    }
}
