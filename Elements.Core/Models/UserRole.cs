﻿using System.Collections.Generic;

namespace Elements.Core.Models
{
    public static class UserRole
    {
        public static string Student = "Estudiante";
        public static string Teacher = "Maestro";
        public static string Admin = "Admin";
        public static string SuperAdmin = "SuperAdmin";

        public static List<string> GetAllRoles()
        {
            return new List<string> {Student, Teacher, Admin, SuperAdmin};
        }
    }
}