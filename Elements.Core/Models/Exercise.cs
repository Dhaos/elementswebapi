﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Elements.Core.Models
{
    public class Exercise
    {   public int Id { get; set; }
        public string Name { get; set; }
        public int StageId { get; set; }
        public Stage Stage { get; set; }
    }
}
