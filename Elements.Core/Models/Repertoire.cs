﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Elements.Core.Models
{
    public class Repertoire
    {   public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string Difficulty { get; set; }
        public string ProficiencyType { get; set; }

        public string Link { get; set; }

        public int PriceInVinyls { get; set; }
    }
}
