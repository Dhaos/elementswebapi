﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Elements.Core.Models
{
    public class Order
    {   public int Id { get; set; }
        public string ReceiptId { get; set; }
        public string Concept { get; set; }
        public decimal? AmountInUSD { get; set; }

        public string SubscriptionId { get; set; }

        public DateTimeOffset Date { get; set; }
        
        public Student Student { get; set; }
        public int StudentId { get; set; }

    }
}
