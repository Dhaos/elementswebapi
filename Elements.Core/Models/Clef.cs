﻿namespace Elements.Core.Models
{
    public static class Clef
    {
        public static string G = "G";
        public static string F = "F";

        public static bool IsValidClef(string clef)
        {
            return clef != null && (clef.ToLowerInvariant() == Clef.G.ToLowerInvariant() || clef.ToLowerInvariant() == Clef.F.ToLowerInvariant());
        }
    }
}