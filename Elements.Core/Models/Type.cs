﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elements.Core.Models
{
    public class Tutorial
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Type { get; set; }
       
    }

   
}
