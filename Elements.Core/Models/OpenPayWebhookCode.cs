﻿namespace Elements.Core.Models
{
    public class OpenPayWebhookCode
    {
        public string Code { get; set; }
    }
}