﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Elements.Core.Models
{
    public class StudentArcadeGame
    {   
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int ArcadeGameId { get; set; }
        public ArcadeGame ArcadeGame { get; set; }

        public decimal Score { get; set; }
        public decimal LevelAchieved { get; set; }
        public DateTimeOffset? Date { get; set; }
        public int TimesPlayed { get; set; }
        
    }
}
