﻿namespace Elements.Core.Models
{
    public static class ReadingSystem
    {
        public static string Solfege = "Solfege";
        public static string Letter = "Letters";

        public static bool IsValidReadingSystem(string system)
        {
            return system != null && (system.ToLowerInvariant() == ReadingSystem.Solfege.ToLowerInvariant() || system.ToLowerInvariant() == ReadingSystem.Letter.ToLowerInvariant());
        }
    }
}