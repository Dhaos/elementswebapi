﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Elements.Core.Models
{
    public class StudentStage
    {
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int StageId { get; set; }
        public Stage Stage { get; set; }
        public int Attempt { get; set; }

        public decimal MaxCombo { get; set; }
        public string ReadingSystem { get; set; }
        public string Language { get; set; }
        public decimal Grade { get; set; }
        public decimal Score { get; set; }
        public int VinylsObtained { get; set; }

        public decimal? ReadingProficiency { get; set; }
        public decimal? TheoryProficiency { get; set; }
        public decimal? ListeningProficiency { get; set; }
        public decimal? RhythmProficiency { get; set; }

        public DateTimeOffset? Date { get; set; }

    }
}
