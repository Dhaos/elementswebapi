﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Elements.Core.Models
{
    public class StudentRepertoire
    {   public int StudentId { get; set; }
        public Student Student { get; set; }
        public int RepertoireId { get; set; }
        public Repertoire Repertoire { get; set; }
        public decimal Score { get; set; }
        public int Rating { get; set; }
        public DateTimeOffset? Date { get; set; }
        public int TimesPlayed { get; set; }
    }
}
