﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Elements.Core.Models
{
    public class School
    {
        public School()
        {
            Classrooms = new List<Classroom>();
            Teachers = new List<Teacher>();
            BlockedStudents = new List<BlockedStudent>();
        }
        public string Name { get; set; }
        public int Id { get; set; }
        public string Logo { get; set; }
        public ICollection<Classroom> Classrooms { get; set; }
        public ICollection<Teacher> Teachers { get; set; }
        public Admin Admin { get; set; }
        public ICollection<BlockedStudent> BlockedStudents { get; set; }

        public int? AllowedStudents { get; set; }

        public bool ActiveRecord { get; set; } = true;
    }
}
