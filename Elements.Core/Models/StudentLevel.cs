﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Elements.Core.Models
{
    public class StudentLevel
    {
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int LevelId { get; set; }
        public Level Level { get; set; }
        
        public bool Unlocked { get; set; }

    }
}
