﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Elements.Core.Interfaces;


namespace Elements.Core.Models
{
    public class Student: IIdentityUser
    {
        public Student()
        {
            ArcadeGames = new List<StudentArcadeGame>();
            Classrooms = new List<StudentClassroom>();
            Repertoires = new List<StudentRepertoire>();
            Stages = new List<StudentStage>();
            Levels = new List<StudentLevel>(); 
            Lessons = new List<StudentLesson>();
            Orders = new List<Order>();

        }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public string ApplicationUserId { get; set; }
        public string PaymentUserId { get; set; }
        public string Username { get; set; }

        public int CountryId { get; set; }
        public Country Country { get; set; }
        public string Clef { get; set; }
        public string ReadingSystem { get; set; }
        public string Language { get; set; }
        public string PictureUrl { get; set; }
        public DateTimeOffset BirthDate { get; set; }

        public DateTimeOffset? ProExipirationDate { get; set; }
        public DateTimeOffset? LastVinylVideoDate { get; set; }

        public bool IsProViaSchool
        {
            get
            {
                if (Classrooms.Any()) return true;
                return false;
            }
        }

        public bool IsPro
        {
            get
            {
                if (ProExipirationDate != null && DateTimeOffset.Now<ProExipirationDate || IsProViaSchool) return true;
                return false;
            }
        }

        public int Vinyls { get; set; }
        public int Tickets { get; set; }

        public ICollection<StudentArcadeGame> ArcadeGames { get; set; }
        public ICollection<StudentClassroom> Classrooms { get; set; }
        public ICollection<StudentRepertoire> Repertoires { get; set; }
        public ICollection<StudentStage> Stages { get; set; }
        public ICollection<StudentLevel> Levels { get; set; }
        public ICollection<StudentLesson> Lessons { get; set; }
        public ICollection<Order> Orders { get; set; }
        public string ShopToken { get; set; }
    }
}
