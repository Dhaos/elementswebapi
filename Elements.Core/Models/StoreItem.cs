﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elements.Core.Models
{
    public class StoreItem
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public string Concept { get; set; }
        public decimal AmountInUSD { get; set; }
        public StoreItemType Type { get; set; }
        public StoreItemKind Kind { get; set; }
        public string SubscriptionId { get; set; }
    }

    public enum StoreItemType
    {
        Purchase,
        Subscription
    }

    public enum StoreItemKind
    {
        Vinyls,
        Pro
    }
}
