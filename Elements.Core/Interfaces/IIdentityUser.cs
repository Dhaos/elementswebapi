﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elements.Core.Interfaces
{
    public interface IIdentityUser : IProfile
    {
        int Id { get; set; }
        string ApplicationUserId { get; set; }

    }

    public interface IProfile
    {
        string Name { get; }
        string Email { get; set; }
    }
}
