﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elements.Core.Interfaces

{
    public interface ISoftDelete
    {
        bool ActiveRecord { get; set; }
    }
}
