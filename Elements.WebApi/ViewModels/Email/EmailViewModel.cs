﻿using MimeKit;

namespace Elements.WebApi.ViewModels
{
    public class EmailViewModel
    {
        /// <summary>Nombre del destinatario.</summary>
        public string Name { get; set; }
        /// <summary>Correo electrónico del destinatario</summary>
        public string Email { get; set; }
        /// <summary>Cuerpo del mensaje.</summary>
        public MimeMessage Message { get; set; }
        /// <summary>Enlace del mensaje.</summary>
        public string Link { get; set; }
    }
}
