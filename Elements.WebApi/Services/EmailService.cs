﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Elements.Core.Models;
using Elements.WebApi.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Elements.WebApi.Services
{
    public class EmailService
    {
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _environment;

        public EmailService(IConfiguration configuration, IHostingEnvironment environment)
        {
            _configuration = configuration;
            _environment = environment;
        }
        public SendGridMessage CreateEmail()
        {
            var msg = new SendGridMessage();

            msg.SetFrom(new EmailAddress("dx@example.com", "SendGrid DX Team"));

            var recipients = new List<EmailAddress>
            {
                new EmailAddress("jeff@example.com", "Jeff Smith"),
                new EmailAddress("anna@example.com", "Anna Lidman"),
                new EmailAddress("peter@example.com", "Peter Saddow")
            };
            msg.AddTos(recipients);

            msg.SetSubject("Testing the SendGrid C# Library");

            msg.AddContent(MimeType.Text, "Hello World plain text!");
            msg.AddContent(MimeType.Html, "<p>Hello World!</p>");

            return msg;
        }

        private void AddLogoToMessage(SendGridMessage msg)
        {
            var logo = Path.Combine(_environment.WebRootPath, "images", "logo.png");
            var logoFile = Convert.ToBase64String(File.ReadAllBytes(logo));
            var attachments = new List<Attachment>()
            {
                new Attachment()
                {
                    Content = logoFile,
                    Type = "image/png",
                    Filename = "logo.png",
                    Disposition = "inline",
                    ContentId = "Logo"
                }
            };
            msg.AddAttachments(attachments);
        }

        public SendGridMessage CreateWelcomeEmail(string email, string name, string language = "spanish")
        {
            var msg = new SendGridMessage();

            msg.SetFrom(new EmailAddress("info@elementsme.com", "Elements ME"));

            var recipients = new List<EmailAddress>
            {
                new EmailAddress(email, name)
            };
            msg.AddTos(recipients);

            msg.SetSubject("Welcome to Elements ME!");
            AddLogoToMessage(msg);
            msg.AddContent(MimeType.Html, GenerateWelcomeHtml(email, name, language));

            return msg;
        }

        public SendGridMessage CreateResetPasswordEmail(string email, string resetUrl, string language = "spanish")
        {
            var msg = new SendGridMessage();

            msg.SetFrom(new EmailAddress("info@elementsme.com", "Elements ME"));

            var recipients = new List<EmailAddress>
            {
                new EmailAddress(email)
            };
            msg.AddTos(recipients);

            msg.SetSubject("Reset password request from Elements ME");
            AddLogoToMessage(msg);
            msg.AddContent(MimeType.Html, GenerateResetHtml(email, resetUrl, language));

            return msg;
        }

        public SendGridMessage CreatePaymentCompletedEmail(string email, string name)
        {
            var msg = new SendGridMessage();

            msg.SetFrom(new EmailAddress("info@elementsme.com", "Elements ME"));

            var recipients = new List<EmailAddress>
            {
                new EmailAddress(email)
            };
            msg.AddTos(recipients);

            msg.SetSubject("Your order has been completed");

            AddLogoToMessage(msg);
            msg.AddContent(MimeType.Html, GeneratePaymentHtml(email, name));

            return msg;
        }

        public async Task<Response> SendMail(SendGridMessage message)
        {
            var apiKey = _configuration.GetValue<string>("SendGridApiKey");
            var client = new SendGridClient(apiKey);
            return await client.SendEmailAsync(message);
        }


        private string GenerateTestWelcomeHtml(string username, string name, string language = "spanish")
        {
            var html = "";
            if (language.ToLower() == "spanish")
            {
                html = $@"<!DOCTYPE html>
                <html>
                {GetHeadTag()}
                <body>
                    <div class=""content"">
	                    <div class=""container"">
	                      <img src=""cid:Logo"" width=""150px"">
	                      <h1>¡Hola {name}!</h1>
	                      <div class=""message-holder"">
	                        <p class=""normal-content"">Tu usuario registrado es:</p>
	                        <p class=""normal-bold-content"">{username}</p> 
	                      </div>  
	                    </div>
	                    <p class=""muted-content"">
		                    No respondas a este mensaje, esta es solo una cuenta para envío. Si deseas contactar con nosotros escribe a REDACTED.
	                    </p>
                    </div>
                </body>
                </html>";
            }
            else {
                html = $@"<!DOCTYPE html>
                <html>
                {GetHeadTag()}
                <body>
                    <div class=""content"">
	                    <div class=""container"">
	                      <img src=""cid:Logo"" width=""150px"">
	                      <h1>¡Hello {name}!</h1>
	                      <div class=""message-holder"">
	                        <p class=""normal-content"">Your registered username is:</p>
	                        <p class=""normal-bold-content"">{username}</p> 
	                      </div>  
	                    </div>
	                    <p class=""muted-content"">
		                    Do not reply to this message, this is a send-only email account. If you wish to contact us, please write to: #####.
	                    </p>
                    </div>
                </body>
                </html>";

            }

            return html;

        }


        private string GenerateWelcomeHtml(string username, string name, string language = "spanish")
        {
            var html = "";
            var usernameLine = language.ToLower() == "spanish"
                ? $"Tu usuario registrado es {username}"
                : $"Your registered username is {username}.";
            var welcomeLine = language.ToLower() == "spanish"
                ? $"Bienvenido a Elements Musical Experience!"
                : $"Welcome to Elements Musical Experience!";

            html = $@"<!DOCTYPE html>
                <html>
                {GetTestHeadTag()}
                <body>
      <center class=""wrapper"" data-link-color=""#993300"" data-body-style=""font-size:16px; font-family:verdana,geneva,sans-serif; color:#516775; background-color:#F9F5F2;"">
        <div class=""webkit"">
          <table cellpadding=""0"" cellspacing=""0"" border=""0"" width=""100%"" class=""wrapper"" bgcolor=""#F9F5F2"">
            <tbody><tr>
              <td valign=""top"" bgcolor=""#F9F5F2"" width=""100%"">
                <table width=""100%"" role=""content-container"" class=""outer"" align=""center"" cellpadding=""0"" cellspacing=""0"" border=""0"">
                  <tbody><tr>
                    <td width=""100%"">
                      <table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0"">
                        <tbody><tr>
                          <td>
                            <!--[if mso]>
    <center>
    <table><tr><td width=""600"">
  <![endif]-->
                                    <table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width:100%; max-width:600px;"" align=""center"">
                                      <tbody><tr>
                                        <td role=""modules-container"" style=""padding:0px 0px 0px 0px; color:#516775; text-align:left;"" bgcolor=""#78A5D8"" width=""100%"" align=""left""><table class=""module preheader preheader-hide"" role=""module"" data-type=""preheader"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;"">
    <tbody><tr>
      <td role=""module-content"">
        <p>{welcomeLine}</p>
      </td>
    </tr>
  </tbody></table><table class=""wrapper"" role=""module"" data-type=""image"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""table-layout: fixed;"" data-muid=""qa8oMphYHuL7xyQrTVscgD"">
      <tbody><tr>
        <td style=""font-size:6px; line-height:10px; padding:30px 0px 0px 0px;"" valign=""top"" align=""center"">
          <img class=""max-width"" border=""0"" style=""display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px; max-width:50% !important; width:50%; height:auto !important;"" src=""cid:Logo"" alt=""Elements Musical Experience"" width=""300"" data-responsive=""true"" data-proportionally-constrained=""false"">
        </td>
      </tr>
    </tbody></table><table class=""module"" role=""module"" data-type=""spacer"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""table-layout: fixed;"" data-muid=""bdzDb4B4pnnez4W7L1KpxJ"">
      <tbody><tr>
        <td style=""padding:0px 0px 30px 0px;"" role=""module-content"" bgcolor="""">
        </td>
      </tr>
    </tbody></table><table class=""module"" role=""module"" data-type=""text"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""table-layout: fixed;"" data-muid=""gNWHzBzkFeWH4JDKd2Aikk"">
      <tbody><tr>
        <td style=""background-color:#ffffff; padding:50px 0px 10px 0px; line-height:30px; text-align:inherit;"" height=""100%"" valign=""top"" bgcolor=""#ffffff""><div><div style=""font-family: inherit; text-align: center""><span style=""color: #516775; font-size: 28px; font-family: georgia,serif""><strong>{welcomeLine}</strong></span></div><div></div></div></td>
      </tr>
    </tbody></table><table class=""module"" role=""module"" data-type=""text"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""table-layout: fixed;"" data-muid=""bA2FfEE6abadx6yKoMr3F9"">
      <tbody><tr>
        <td style=""background-color:#ffffff; padding:10px 40px 50px 40px; line-height:22px; text-align:inherit;"" height=""100%"" valign=""top"" bgcolor=""#ffffff""><div><div style=""font-family: inherit; text-align: center""><span style=""font-family: verdana,geneva,sans-serif"">
{usernameLine}.
</span></div><div></div></div></td>
      </tr>
    </tbody></table><table class=""module"" role=""module"" data-type=""spacer"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""table-layout: fixed;"" data-muid=""dnNq8YR2nu8DNzse1aZUWt"">
      <tbody><tr>
      </tr>
    </tbody></table><table class=""module"" role=""module"" data-type=""divider"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""table-layout: fixed;"" data-muid=""ei2zeSTvjHYmn1YhKSUfaB"">
      <tbody><tr>
        <td style=""padding:0px 0px 0px 0px;"" role=""module-content"" height=""100%"" valign=""top"" bgcolor="""">
          
        </td>
      </tr>
    </tbody></table><table class=""module"" role=""module"" data-type=""spacer"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""table-layout: fixed;"" data-muid=""vFfA6A3u2gVDK2QbpXDqPo"">
      <tbody><tr>
        
      </tr>
    </tbody></table><table class=""module"" role=""module"" data-type=""text"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""table-layout: fixed;"" data-muid=""2q8x8zTfLywQieSSYmZbus"">
      <tbody><tr>
          </tr>
    </tbody></table><table class=""module"" role=""module"" data-type=""divider"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""table-layout: fixed;"" data-muid=""qkG1GEG4EZSwoAzbwgoD8v"">
      <tbody><tr>
        <td style=""padding:0px 0px 0px 0px;"" role=""module-content"" height=""100%"" valign=""top"" bgcolor="""">
          <table border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" width=""100%"" height=""10px"" style=""line-height:10px; font-size:10px;"">
            <tbody><tr>
              <td style=""padding:0px 0px 10px 0px;"" bgcolor=""#ffffff""></td>
            </tr>
          </tbody></table>
        </td>
      </tr>
    </tbody></table><table class=""module"" role=""module"" data-type=""spacer"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""table-layout: fixed;"" data-muid=""vqDDw7scxs521qMEgEyyuF"">
      <tbody><tr>
        
       
      </tr>
    </tbody></table>
   

                                      </tr>
                                    </tbody></table>
                                    <!--[if mso]>
                                  </td>
                                </tr>
                              </table>
                            </center>
                            <![endif]-->
                          </td>
                        </tr>
                      </tbody></table>
                    </td>
                  </tr>
                </tbody></table>
              </td>
            </tr>
          </tbody></table>
        </div>
      </center>
    
  
</body>
                </html>";

            return html;

        }


        private string GenerateResetHtml(string email, string code, string language = "spanish")
        {
            var html = "";
            if (language.ToLower() == "spanish")
            {
                html = $@"<!DOCTYPE html>
            <html>
            {GetHeadTag()}
            <body>
                <div class=""content"">
	                <div class=""container"">
	                  <img src=""cid:Logo"" width=""150px"">
	                  <h1>Reestablecimiento de contraseña</h1>
	                  <div class=""message-holder"">
	                    <p class=""normal-content"">Por favor reestablece tu contraseña haciendo click <a href={code}>aqui</a> o  en la siguiente liga:</p>
	                    <p class=""normal-bold-content"">{code}</p> 
	                  </div>  
	                </div>
	                <p class=""muted-content"">
		                No respondas a este mensaje, esta es solo una cuenta para envío. Si deseas contactar con nosotros escribe a REDACTED.
	                </p>
                </div>
            </body>
            </html>";

            }
            else
            {
                html = $@"<!DOCTYPE html>
            <html>
            {GetHeadTag()}
            <body>
                <div class=""content"">
	                <div class=""container"">
	                  <img src=""cid:Logo"" width=""150px"">
	                  <h1>Password reset</h1>
	                  <div class=""message-holder"">
	                    <p class=""normal-content"">Please, reset your password clicking <a href={code}>here</a> o the following link:</p>
	                    <p class=""normal-bold-content"">{code}</p> 
	                  </div>  
	                </div>
	                <p class=""muted-content"">
		                 Do not reply to this message, this is a send-only email account. If you wish to contact us, please write to: #####.
	                </p>
                </div>
            </body>
            </html>";
            }

            return html;
        }

       

      
        private string GeneratePaymentHtml(string email, string code)
        {
            var html = $@"<!DOCTYPE html>
            <html>
            {GetHeadTag()}
            <body>
                <div class=""content"">
	                <div class=""container"">
	                  <img src=""cid:Logo"" width=""150px"">
	                  <h1>Pago exitoso</h1>
	                  <div class=""message-holder"">
	                   
	                  </div>  
	                </div>
	                <p class=""muted-content"">
		                No respondas a este mensaje, esta es solo una cuenta para envío. Si deseas contactar con nosotros escribe a REDACTED.
	                </p>
                </div>
            </body>
            </html>";

            return html;
        }

      


        private string GetHeadTag()
        {
            var head = $@"<head>
            <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
            <style>
	            .content{{
	              font-family: Helvetica, Arial;
	              text-align:center;
	            }}
	            .container{{
	              margin:0;
	              padding:40px;  
	              background-color:#F9F8F7;
	              margin-bottom:25px;
	            }}
	            .container h1{{
	              font-size:28px;
	              color:#461220;
	              letter-spacing:0.88px;
	              font-weight:400;
	            }}
	            .container h2{{
	              font-size:16px;
	              color:#461220;
	              width:80%;
	              margin:auto;
	              font-weight:400;
	            }}
	            .container .signature{{
	              font-size:13px;
	              color:#461220;
	              line-height:18px;
	              font-weight:400;
	            }}

	            .message-holder{{
	              background-color:#FFFFFF;
	              border: 1px solid #BFB8AD;
	              padding:20px;
	              margin-top:25px;  
	            }}
	            .message-holder ul{{
	              width:65%;
	              margin:auto;
	              text-align: left;
	            }}
	            .message-holder ul li{{
	              font-size: 15px;
	              color: #998888;
	              line-height: 18px;
	              font-weight: bold;
	            }}

	            .normal-content{{
	                font-size: 15px;
	                color: #998888;
	                line-height: 18px;
	            }}

	            .normal-bold-content{{
	                font-size: 18px;
	                color: #998888;
	                line-height: 18px;
	                font-weight: bold;
	            }}

	            .muted-content{{
	                font-size: 12px;
	                color: #BFB8AD;
	                line-height: 17px;
	            }}

	            @media only screen and (min-width: 600px) {{
	                /* For tablets: */
	              .container h2{{    
	                width:70%;    
	              }}
	              .message-holder ul{{
	                width:45%;    
	              }}
	            }}
	            @media only screen and (min-width: 768px) {{
	                /* For medium: */
	              .container h2{{    
	                width:60%;    
	              }}
	              .message-holder ul{{
	                width:35%;    
	              }}
	            }}

	            @media only screen and (min-width: 992px) {{
	              /* For large and up: */
	              .container h2{{    
	                width:50%;    
	              }}
	              .message-holder ul{{
	                width:30%;    
	              }}
	            }} 
            </style>
            </head>";

            return head;
        }
        private string GetTestHeadTag()
        {
            var head = $@"<head>
      <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">
      <meta name=""viewport"" content=""width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"">
      <!--[if !mso]><!-->
      <meta http-equiv=""X-UA-Compatible"" content=""IE=Edge"">
      <!--<![endif]-->
      <!--[if (gte mso 9)|(IE)]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <!--[if (gte mso 9)|(IE)]>
  <style type=""text/css"">
    body {{width: 600px;margin: 0 auto;}}
    table {{border-collapse: collapse;}}
    table, td {{mso-table-lspace: 0pt;mso-table-rspace: 0pt;}}
    img {{-ms-interpolation-mode: bicubic;}}
  </style>
<![endif]-->
      <style type=""text/css"">
    body, p, div {{
      font-family: verdana,geneva,sans-serif;
      font-size: 16px;
    }}
    body {{
      color: #516775;
    }}
    body a {{
      color: #993300;
      text-decoration: none;
    }}
    p {{ margin: 0; padding: 0; }}
    table.wrapper {{
      width:100% !important;
      table-layout: fixed;
      -webkit-font-smoothing: antialiased;
      -webkit-text-size-adjust: 100%;
      -moz-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }}
    img.max-width {{
      max-width: 100% !important;
    }}
    .column.of-2 {{
      width: 50%;
    }}
    .column.of-3 {{
      width: 33.333%;
    }}
    .column.of-4 {{
      width: 25%;
    }}
    @media screen and (max-width:480px) {{
      .preheader .rightColumnContent,
      .footer .rightColumnContent {{
        text-align: left !important;
      }}
      .preheader .rightColumnContent div,
      .preheader .rightColumnContent span,
      .footer .rightColumnContent div,
      .footer .rightColumnContent span {{
        text-align: left !important;
      }}
      .preheader .rightColumnContent,
      .preheader .leftColumnContent {{
        font-size: 80% !important;
        padding: 5px 0;
      }}
      table.wrapper-mobile {{
        width: 100% !important;
        table-layout: fixed;
      }}
      img.max-width {{
        height: auto !important;
        max-width: 100% !important;
      }}
      a.bulletproof-button {{
        display: block !important;
        width: auto !important;
        font-size: 80%;
        padding-left: 0 !important;
        padding-right: 0 !important;
      }}
      .columns {{
        width: 100% !important;
      }}
      .column {{
        display: block !important;
        width: 100% !important;
        padding-left: 0 !important;
        padding-right: 0 !important;
        margin-left: 0 !important;
        margin-right: 0 !important;
      }}
    }}
  </style>
      <!--user entered Head Start-->

     <!--End Head user entered-->
    </head>";

            return head;
        }

        
    }
}
