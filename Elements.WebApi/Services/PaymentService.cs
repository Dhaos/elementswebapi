﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elements.Core.Models;
using Elements.WebApi.Services;
using Korvi.NetStandard.Openpay;
using Korvi.NetStandard.Openpay.Models;
using Korvi.NetStandard.Openpay.Models.Cards;
using Korvi.NetStandard.Openpay.Models.Charges;
using Korvi.NetStandard.Openpay.Models.Customers;
using Korvi.NetStandard.Openpay.Models.Subscriptions;
using MimeKit;

namespace Elements.WebApi.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly OpenPayApi openpayApi;
        private readonly string merchantId = "mqu53opma7gh7msk2abd"; //SANDBOX
        private readonly string baseReceiptUrl = "https://sandbox-dashboard.openpay.mx/"; //SANDBOX
        private readonly string privateKey = "sk_74ccc11b3e3a46aba5edfab29a1fa1e1"; //SANDBOX


        public PaymentService()
        {
            /* openpayApi = new OpenpayAPI("sk_cb59e622b37a421f858092f5a52b6941", "mkovo9ar72u9yrnore1k");
             openpayApi.Production = false;*/ // SANDBOX

            openpayApi = new OpenPayApi(privateKey, merchantId, false);
        }

        public async Task<CardChargeStatus> Charge(string token, string deviceSessionId, Student user, Order order,
            string cvv2)
        {
            ChargeWithCardIdOrTokenRequest request = new ChargeWithCardIdOrTokenRequest();
            Customer customer = new Customer();
            customer.name = "Jesus Dueñas Acosta";
            customer.creation_date = DateTimeOffset.Now.ToString("O");
            customer.last_name = user.LastName;
            customer.email = user.Email;
            request.source_id = token;
            request.cvv2 = Int32.Parse(cvv2);
            request.amount = order.AmountInUSD ?? 0;
            request.currency = "MXN";
            request.description = order.Concept;
            request.order_id = "EM20-" + user.Id + "-" + order.Date.ToString("yyMMdd") + "-" +
                               new Random().Next(10000);
            request.device_session_id = deviceSessionId;
            request.customer = customer;


            try
            {
                var charge = await openpayApi.ChargeMerchantWithACardIdOrToken(request);
                if (charge.Succeded)
                {
                    var status = new CardChargeStatus();

                    status.Concept = request.description;
                    status.ExternalOrderId = charge.Result.order_id;
                    status.ExternalId = charge.Result.id;
                    status.Type = charge.Result.method;
                    status.LastDigits =
                        charge.Result.card.card_number.Substring(charge.Result.card.card_number.Length - 4);
                    status.Brand = charge.Result.card.brand;
                    status.Success = true;

                    return status;
                }
                else
                {
                    var e = new Exception(charge.Error.description);
                    throw e;
                }

            }
            catch (Exception e)
            {
                return new CardChargeStatus() {Success = false, Message = e.Message};
            }

        }

        public async Task<CardChargeStatus> Subscribe(string token, Student user, Order order, string planId)
        {
            try
            {
                CreateNewSubscriptionRequest request = new CreateNewSubscriptionRequest();
                CreateNewCustomerRequest customer = new CreateNewCustomerRequest();
                customer.name = user.Name;
                //customer.LastName = user.LastName;
                customer.email = user.Email;
                customer.external_id = user.Id.ToString();
                customer.requires_account = false;

                Customer actualCustomer = null;
                var customerResult = (await openpayApi.GetCustomer(user?.PaymentUserId ?? "1"));
                if (customerResult.Succeded)
                {
                    actualCustomer = customerResult.Result;
                }
                else
                {
                    var createCustomerResult = (await openpayApi.CreateCustomer(customer));
                    if (createCustomerResult.Succeded)
                    {
                        actualCustomer = createCustomerResult.Result;
                        user.PaymentUserId = actualCustomer.id;
                    }
                    else
                    {
                        var e = new Exception(createCustomerResult.Error.description);
                        throw e;
                    }

                }

                request.source_id = token;
                request.plan_id = planId;

                var charge = await openpayApi.CreateSubscription(request, actualCustomer.id);
                if (charge.Succeded)
                {
                    var status = new CardChargeStatus();

                    status.Concept = order.Concept;
                    status.ExternalOrderId = charge.Result.id;
                    status.ExternalId = charge.Result.id;
                    status.Type = "subscription";
                    status.LastDigits =
                        charge.Result.card.card_number.Substring(charge.Result.card.card_number.Length - 4);
                    status.Brand = charge.Result.card.brand;
                    status.CustomerId = actualCustomer.id;
                    status.Success = true;

                    return status;
                }
                else
                {
                    var e = new Exception(charge.Error.description);
                    throw e;
                }

            }
            catch (Exception e)
            {
                return new CardChargeStatus() {Success = false, Message = e.Message};
            }

        }


        /* public ShopChargeStatus ChargeShop(Student user, Order order)
         {
             ChargeViaStoreRequest request = new ChargeViaStoreRequest();
             CreateNewCustomerRequest customer = new CreateNewCustomerRequest();

             customer.name = user.Name;
             //customer.LastName = user.LastName;
             customer.email = user.Email;

             request.amount = order.AmountInUSD ?? 0;
             request.description = $"Cargo en tienda: {order.Concept}";
             request.order_id = "EM20-" + user.Id + "-" + order.Date.ToString("yyMMdd") + "-" +
                                new Random().Next(10000);
             request.SetDueDate(DateTime.Now.AddDays(2));
             request.customer = customer;


             try
             {
                 var charge = openpayApi.ChargeService.Create(request);
                 var status = new ShopChargeStatus();
                 status.BarcodeUrl = charge.PaymentMethod.BarcodeURL;
                 status.Concept = request.Description;
                 status.Url = charge.PaymentMethod.Url;
                 status.Type = charge.Method;
                 status.Reference = charge.PaymentMethod.Reference;
                 status.ReceiptLink = baseReceiptUrl + "paynet-pdf/" + merchantId + "/" + status.Reference;
                 status.DueDate = request.DueDate.Value;
                 status.ExternalId = charge.Id;
                 status.ExternalOrderId = charge.OrderId;
                 status.Success = true;
                 return status;
             }
             catch (OpenpayException exception)
             {
                 return new ShopChargeStatus { Success = false, Message = exception.Message };
             }

         }
         */
        /*public BankChargeStatus ChargeBank(Student user, Order order)
        {
            ChargeRequest request = new ChargeRequest();
            Customer customer = new Customer();
            customer.Name = user.Name;
            //customer.LastName = user.LastName;
            customer.Email = user.Email;

            request.Method = "bank_account";
            request.Amount = order.AmountInUSD ?? 0;
            request.Currency = "MXN";
            request.Description = $"Cargo en banco: {order.Concept}";
            request.OrderId = request.OrderId = "EM22-" + user.Id + "-" + order.Date.ToString("yyMMdd") + "-" +
                                                new Random().Next(10000);
            request.DueDate = DateTime.Now.AddDays(2);
            request.Customer = customer;


            try
            {
                var charge = openpayApi.ChargeService.Create(request);
                var status = new BankChargeStatus();
                status.Clabe = charge.PaymentMethod.CLABE;
                status.Concept = request.Description;
                status.Type = charge.Method;
                status.Reference = charge.PaymentMethod.Reference;
                status.Agreement = charge.PaymentMethod.Agreement;
                status.ReceiptLink = baseReceiptUrl + "spei-pdf/" + merchantId + "/" + charge.Id;
                status.DueDate = request.DueDate.Value;
                status.ExternalOrderId = charge.OrderId;
                status.ExternalId = charge.Id;
                status.BankName = charge.PaymentMethod.BankName;
                status.Name = charge.PaymentMethod.Name;
                status.Success = true;
                return status;

            }
            catch (OpenpayException exception)
            {
                return new BankChargeStatus { Success = false, Message = exception.Message };
            }

        }
      */
        public async Task<CardValidationTemplate> GetCard(string id)
        {
            try
            {
                RequestResult<Card> card = await openpayApi.GetMerchantCard(id);
                if (card.Succeded)
                {
                    var cardResult = card.Result;
                    CardValidationTemplate tarjeta = new CardValidationTemplate
                    {
                        Id = cardResult.id,
                        Brand = cardResult.brand,
                        CardNumber = cardResult.card_number,
                        HolderName = cardResult.holder_name,
                        ExpirationYear = cardResult.expiration_year,
                        ExpirationMonth = cardResult.expiration_month
                    };
                    return tarjeta;
                }
                else
                {
                    throw new Exception();
                }


            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<bool> DeleteCard(string id)
        {
            try
            {
                var result = await openpayApi.DeleteMerchantCard(id);
                if (result.Succeded)
                {
                    return true;
                }

                return false;

            }
            catch (Exception ex)
            {
                return false;
            }
        }



        public async Task<string> CreateCard(string token, string deviceSessionId)
        {
            CreateNewCardWithTokenRequest card = new CreateNewCardWithTokenRequest();
            card.token_id = token;
            card.device_session_id = deviceSessionId;

            try
            {
                var cardDb = await openpayApi.CreateMerchantCardWithToken(card);
                if (cardDb.Succeded)
                {
                    return cardDb.Result.id;
                }
                else
                {
                    throw new Exception();
                }

            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<ICollection<SubscriptionStatus>> GetSubscription(Student student)
        {
            if (!string.IsNullOrWhiteSpace(student.PaymentUserId))
            {
                var subscriptionsRequest = await openpayApi.GetSubscriptions(student.PaymentUserId);
                if (subscriptionsRequest.Succeded)
                    return subscriptionsRequest.Result.Select(x => new SubscriptionStatus
                    {
                        Id = x.id,
                        ChargeDate = x.ChargeDate,
                        EndDate = x.PeriodEndDate,
                        CreationDate = x.CreationDate,
                        Status = x.status
                    }).ToList();
                else
                {
                    return new List<SubscriptionStatus>();
                }
            }
            else return new List<SubscriptionStatus>();
        }
        }


        public class SubscriptionStatus
        {
            public string Status { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? ChargeDate { get; set; }
            public DateTime? CreationDate { get; set; }
            public string Id { get; set; }

        }

        public class ChargeStatus
        {
            public bool Success { get; set; }
            public string Message { get; set; }
            public DateTimeOffset DueDate { get; set; }
            public string ExternalId { get; set; }
            public string ExternalOrderId { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public string Concept { get; set; }
            public string ReceiptLink { get; set; }
            public string CustomerId { get; set; }

        }

        public class CardChargeStatus : ChargeStatus
        {
            public string Brand { get; set; }
            public string LastDigits { get; set; }
        }

        public class BankChargeStatus : ChargeStatus
        {
            public string Clabe { get; set; }
            public string Reference { get; set; }
            public string Agreement { get; set; }
            public string BankName { get; set; }


        }

        public class ShopChargeStatus : ChargeStatus
        {
            public string BarcodeUrl { get; set; }
            public string Reference { get; set; }
            public string Url { get; set; }

        }

        public class CardValidationTemplate
        {
            public string Id { get; set; }
            public string HolderName { get; set; }
            public string CardNumber { get; set; }
            public string ExpirationMonth { get; set; }
            public string ExpirationYear { get; set; }
            public string Brand { get; set; }
        }


    public interface IPaymentService
    {
        Task<CardChargeStatus> Charge(string token, string deviceSessionId, Student user, Order order, string cvv2);
        Task<string> CreateCard(string token, string deviceSessionId);
        Task<CardValidationTemplate> GetCard(string id);

        Task<bool> DeleteCard(string id);
        //ShopChargeStatus ChargeShop(Student user, Order order);
        //BankChargeStatus ChargeBank(Student user, Order order);
    }
}


