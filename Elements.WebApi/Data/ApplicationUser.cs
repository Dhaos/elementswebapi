﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elements.Core.Models;
using Microsoft.AspNetCore.Identity;

namespace Elements.WebApi.Data
{

    public class ApplicationUser : IdentityUser
    {
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
