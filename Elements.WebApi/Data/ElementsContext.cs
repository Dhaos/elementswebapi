﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elements.Core;
using Elements.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Zaza.WebApi.Controllers;

namespace Elements.WebApi.Data
{
    public class ElementsContext : IdentityDbContext<ApplicationUser>
    {
        public ElementsContext(DbContextOptions<ElementsContext> options)
            : base(options)
        {
        }

        public ElementsContext()
        {
        }

        public DbSet<Admin> Admins { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<StoreItem> StoreItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Classroom> Classrooms { get; set; }
        public DbSet<StudentLevel> StudentLevel { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Stage> Stages { get; set; }
        public DbSet<StudentStage> StudentStage { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<StudentLesson> StudentLesson { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<ArcadeGame> ArcadeGames { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Repertoire> Repertoires { get; set; }
        public DbSet<Tutorial> Tutorials { get; set; }
        public DbSet<OpenPayWebhookCode> OpenPayWebhookCode { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OpenPayWebhookCode>().HasKey(x => x.Code);
            modelBuilder.Entity<Stage>().HasIndex(x => new {x.LessonId, x.OrderInLesson});
            modelBuilder.Entity<BlockedStudent>().HasKey(x => new {x.StudentId, x.SchoolId});
            modelBuilder.Entity<StudentLesson>().HasKey(x => new {x.StudentId, x.LessonId});
            modelBuilder.Entity<StudentLevel>().HasKey(x => new {x.StudentId, x.LevelId});
            modelBuilder.Entity<StudentArcadeGame>().HasKey(x => new {x.StudentId, x.ArcadeGameId});
            modelBuilder.Entity<StudentClassroom>().HasKey(x => new {x.StudentId, x.ClassroomId});
            modelBuilder.Entity<StudentRepertoire>().HasKey(x => new {x.StudentId, x.RepertoireId});
            modelBuilder.Entity<StudentStage>().HasKey(x => new {x.StudentId, x.StageId, x.Attempt});
            modelBuilder.Entity<TeacherClassroom>().HasKey(x => new {x.TeacherId, x.ClassroomId});
            base.OnModelCreating(modelBuilder);
        }
       
    }
}
