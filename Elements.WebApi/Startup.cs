using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Elements.Core.Models;
using Elements.WebApi.Data;
using Elements.WebApi.IdentityServer;
using Elements.WebApi.Services;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Elements.WebApi
{
    public class Startup
    {
        private const string SecretKey = "dxBhfVmBwJft3XQx42WKwgooV6gwQ2zb";

        private readonly SymmetricSecurityKey
        _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));

        private IWebHostEnvironment _env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            _env = env;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors();

            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });

            services.Configure<ApiBehaviorOptions>(options => { options.SuppressModelStateInvalidFilter = true; });

            services.AddIdentity<ApplicationUser, IdentityRole>(o =>
                {
                    o.User.RequireUniqueEmail = true; 
                    o.Password.RequiredLength = 6;
                    o.Password.RequireDigit = false;
                    o.Password.RequireLowercase = false;
                    o.Password.RequireNonAlphanumeric = false;
                    o.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<ElementsContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
                    x.DefaultScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
                })
                .AddIdentityServerAuthentication(
                    x =>
                    {
                        x.Authority = "http://localhost:5001";
                        //x.Authority = "https://elementsv2webapi20200506170907.azurewebsites.net";
                        x.ApiName = "elements.api";
                        x.RequireHttpsMetadata = false;
                    });
            var localEnv = Configuration.GetValue<string>("Environment");
            string connString = Configuration.GetValue<string>("ConnectionStrings:" + localEnv);
            services.AddDbContext<ElementsContext>(options => options.UseSqlServer(connString));
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            var cert = new X509Certificate2(Path.Combine(_env.ContentRootPath, "Cert\\[wildcard].check-in.mx.pfx"),
                "password",
                X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);

            services.AddIdentityServer().AddSigningCredential(cert)
                .AddOperationalStore(options =>
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(connString,
                            sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly)))
                .AddConfigurationStore(options =>
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(connString,
                            sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly)))
                .AddAspNetIdentity<ApplicationUser>();

            services.AddTransient<IResourceOwnerPasswordValidator, JwtClaimsValidator>();
            services.AddTransient<IProfileService, ProfileService>();
            services.AddTransient<PaymentService, PaymentService>();
            services.AddTransient<EmailService, EmailService>();

            services.AddMvc(option => option.EnableEndpointRouting = false)
                .AddNewtonsoftJson(opts =>
                {
                    opts.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    opts.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", info: new OpenApiInfo()
                {

                    Title = "Elements",
                    Version = "v1",
                    Description = "",

                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //c.IncludeXmlComments(xmlPath);
                c.EnableAnnotations();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(builder => { builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseDeveloperExceptionPage();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();
            app.UseWhen(x => !x.Request.Path.Value.StartsWith("/api"), builder =>
            {
                builder.Use(async (context, next) =>
                    {
                        await next();
                        if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                        {
                            context.Request.Path = "/index.html";
                            await next();
                        }
                    })
                    .UseDefaultFiles(new DefaultFilesOptions { DefaultFileNames = new List<string> { "index.html" } })
                    .UseStaticFiles()
                    .UseMvc();
            });
            InitializeDatabase(app);

            app.UseRouting();

            app.UseAuthentication();
            app.UseIdentityServer();
            app.UseMvc();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {

                var localContext = scope.ServiceProvider.GetService<ElementsContext>();

                var roleStore = new RoleStore<IdentityRole>(localContext);
                foreach (string role in UserRole.GetAllRoles())
                {

                    if (!localContext.Roles.Any(r => r.Name == role.ToString()))
                    {
                        await roleStore.CreateAsync(new IdentityRole
                        {
                            Name = role.ToString(),
                            NormalizedName = role.ToString().ToUpper()
                        });
                    }
                }
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Elements V1");
            });

        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var localEnv = Configuration.GetValue<string>("Environment");
                if (localEnv == "Development" || localEnv == "Staging")
                {

                }

                scope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

                var context = scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                context.Database.Migrate();
                if (!context.Clients.Any())
                {
                    foreach (var client in Config.GetClients())
                    {
                        context.Clients.Add(client.ToEntity());
                    }

                    context.SaveChanges();
                }

                if (!context.ApiResources.Any())
                {
                    foreach (var contextScope in Config.GetApiResources())
                    {
                        context.ApiResources.Add(contextScope.ToEntity());
                    }

                    context.SaveChanges();
                }
            }
        }
    }
}
