﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elements.Core.Models;
using Microsoft.AspNetCore.Mvc;

namespace Elements.WebApi.Helpers
{
    public static class EmailHelper
    {
        public static string LinkReset(this ControllerBase controller, string Scheme, string UriComponent, string Role, string Code, string Email)
        {

            return $"{Scheme}://{UriComponent}/access/reset?code={Code}&email={Email}";

        }
    }
}
