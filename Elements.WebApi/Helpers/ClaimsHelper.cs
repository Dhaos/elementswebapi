﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Elements.WebApi.Helpers
{
    public static class ClaimsHelper
    {
        public static int GetUserId(this ControllerBase controller)
        {
            var value = controller.User.FindFirst("id")?.Value;
            if (value != null)
            {
                return int.Parse(value);
            }
            return 0;
        }

        public static string GetApplicationUserId(this ControllerBase controller)
        {
            var value = controller.User.FindFirst("appId")?.Value;
            if (value != null)
            {
                return value;
            }

            return string.Empty;
        }

        public static int GetClinicId(this ControllerBase controller)
        {
            var value = controller.User.FindFirst("clinicId")?.Value;
            if (value != null)
            {
                return int.Parse(value);
            }
            return 0;
        }

        public static string GetName(this ControllerBase controller)
        {
            var roles = controller.User.FindAll(c => c.Type == "name").Select(c => c.Value);
            return roles.FirstOrDefault();
        }

        public static IEnumerable<string> GetRoles(this ControllerBase controller)
        {
            var roles = controller.User.FindAll(c => c.Type == "role").Select(c => c.Value);
            return roles.ToList();
        }

        public static bool IsRole(this ControllerBase controller, string role)
        {
            var roles = controller.GetRoles();
            return roles.Contains(role);
        }

        public static IEnumerable<string> GetPermissions(this ControllerBase controller)
        {
            var permissions = controller.User.FindAll(c => c.Type == "permission").Select(c => c.Value);
            return permissions.ToList();
        }
    }

}
