﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Elements.WebApi.Helpers
{
    public static class CustomResponseHelpers
    {
        public static ActionResult CustomOk(this ControllerBase controller, object value)
        {
            var obj = new { meta = new { code = "200", status = "Success" }, data = value };
            return controller.Ok(obj);
        }

        public static ActionResult CustomOk(this ControllerBase controller)
        {
            var obj = new { meta = new { code = "200", status = "Success" }, data = new { } };
            return controller.Ok(obj);
        }

        public static ActionResult CustomBadRequest(this ControllerBase controller, string error)
        {
            var obj = new { meta = new { code = "400", status = error }, data = new int[] { } };
            return controller.BadRequest(obj);
        }


    }

}
