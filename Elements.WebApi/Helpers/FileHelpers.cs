﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Elements.WebApi.Helpers
{
    public static class FileHelpers
    {

        public static string SaveFile(this ControllerBase controller, string path, string webRootPath, IFormFile file)
        {
            string fullPath = webRootPath + path;
            if (!Directory.Exists(webRootPath + @"/files"))
            {
                Directory.CreateDirectory(webRootPath + @"/files");
            }

            using (var stream = file.OpenReadStream())
            {
                stream.CopyTo(new FileStream(fullPath, FileMode.Create));
            }

            return path;
        }

        public static void DeleteFile(this ControllerBase controller, string path, string webRootPath)
        {
            string fullPath = webRootPath + path;

            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
        }
    }
}
