using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Elements.WebApi.IdentityServer;
using Elements.Core.Interfaces;
using Elements.Core.Models;
using Elements.WebApi.Data;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Elements.WebApi.IdentityServer
{
    public class JwtClaimsValidator : IResourceOwnerPasswordValidator
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ILogger _logger;

        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ElementsContext _context;

        public JwtClaimsValidator(IOptions<JwtIssuerOptions> jwtOptions, ILoggerFactory loggerFactory, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            ElementsContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _jwtOptions = jwtOptions.Value;
            ThrowIfInvalidOptions(_jwtOptions);

            _logger = loggerFactory.CreateLogger<JwtClaimsValidator>();


        }
        /// <summary>
        /// Valida las credenciales del usuario con los parametros necesarios
        /// </summary>
        /// <param name="context">Credenciales del usuario</param>
        /// <returns>Contexto validado</returns>
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var applicationUser = new ApplicationUser { UserName = context.UserName, Password = context.Password };
            var identity = await GetClaimsIdentity(applicationUser);
            if (identity == null)
            {
                _logger.LogInformation($"Invalid username ({applicationUser.UserName}) or password");
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, "El usuario o contrase�a es inv�lido. Por favor, intenta nuevamente.");
                return;
            }


            if (identity.FindFirst("active")?.Value == "False")
            {
                _logger.LogInformation($"({applicationUser.UserName}) is disabled.");
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, "Usuario desactivado");
                return;
            }


            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, applicationUser.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat,
                          ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(),
                          ClaimValueTypes.Integer64),

               
                 identity.FindFirst("name"),
                 identity.FindFirst("firstName"),
                 identity.FindFirst("lastName"),
                 identity.FindFirst("id"),
                 identity.FindFirst("appId"),
                 identity.FindFirst("schoolId"),
                 identity.FindFirst("schoolName"),
                 identity.FindFirst("schoolLogo"),
                 identity.FindFirst("pictureUrl"),
                 identity.FindFirst("email"),



            };
            claims.AddRange(identity.FindAll(c => c.Type == "role"));
            claims.AddRange(identity.FindAll(c => c.Type == "permission"));

            var claimsArray = claims.ToArray();
            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                claims: claimsArray,
                notBefore: _jwtOptions.NotBefore,
                expires: _jwtOptions.Expiration,
                signingCredentials: _jwtOptions.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            // Serialize and return the response
            var response = new Dictionary<string, object>();
            response["role"] = identity.FindAll(c => c.Type == "role").Select(c => c.Value);
            response["permission"] = identity.FindAll(c => c.Type == "permission").Select(c => c.Value);
            response["name"] = identity.FindFirst("name")?.Value ?? "";
            response["firstName"] = identity.FindFirst("firstName")?.Value ?? "";
            response["lastName"] = identity.FindFirst("lastName")?.Value ?? "";
            response["id"] = identity.FindFirst("id")?.Value;
            response["schoolId"] = identity.FindFirst("schoolId")?.Value ?? "";
            response["schoolName"] = identity.FindFirst("schoolName")?.Value ?? "";
            response["schoolLogo"] = identity.FindFirst("schoolLogo")?.Value ?? "";
            response["appId"] = identity.FindFirst("appId")?.Value ?? "";
            response["email"] = identity.FindFirst("email")?.Value ?? "";
            response["birthDate"] = identity.FindFirst("birthDate")?.Value ?? "";

            // var json = JsonConvert.SerializeObject(response, _serializerSettings);

            context.Result = new GrantValidationResult(subject: identity.FindFirst("appId").Value,
                /*response["id"].ToString(),*/ authenticationMethod: "pwd", claims: claimsArray, customResponse: response);
            return;
        }



        /// <summary>
        /// Opciones invalidas de token
        /// </summary>
        /// <param name="options">Opciones de token</param>


        private static void ThrowIfInvalidOptions(JwtIssuerOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (options.ValidFor <= TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(JwtIssuerOptions.ValidFor));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.SigningCredentials));
            }

            if (options.JtiGenerator == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.JtiGenerator));
            }
        }

        /// <summary>
        /// Convierte una fecha a Tiempo Unix (segundos)
        /// </summary>
        /// <param name="date">La fecha.</param>
        /// <returns>Fecha convertida a tiempo Unix</returns>
        private static long ToUnixEpochDate(DateTime date)
          => (long)Math.Round((date.ToUniversalTime() -
                               new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
                              .TotalSeconds);


        /// <summary>
        /// Valida las credenciales del usuario y regresa sus propiedades y permisos
        /// </summary>
        /// <param name="user">Datos del usuario</param>
        /// <returns>Identidad del usuario</returns>
        private async Task<ClaimsIdentity> GetClaimsIdentity(ApplicationUser user)
        {
            var userDb = await _userManager.FindByNameAsync(user.UserName);
            if (userDb == null)
            {
                userDb = _context.Users.FirstOrDefault(u => u.PhoneNumber == user.UserName || u.NormalizedEmail == user.UserName.ToUpperInvariant());
            }
            var result = await _userManager.CheckPasswordAsync(userDb, user.Password);
            string role = null;
            var identity = new ClaimsIdentity();

            IIdentityUser identityUser = null;
            if (userDb != null && result)
            {
                var stringRole = (await _userManager.GetRolesAsync(userDb)).FirstOrDefault()?.ToString();
                if (stringRole != null)
                {
                    role = stringRole;
                    switch (role)
                    {

                        case "Admin":
                        case "SuperAdmin":
                            var admin = _context.Admins.FirstOrDefault(u =>
                                u.ApplicationUserId == userDb.Id);
                            identityUser = admin as IIdentityUser;
                            identity.AddClaim(new Claim("email", admin?.Email));
                            identity.AddClaim(new Claim("schoolId", admin?.SchoolId.ToString()));
                            var school = _context.Schools.FirstOrDefault(x => x.Id == admin.SchoolId);
                            identity.AddClaim(new Claim("schoolName", school?.Name ?? ""));
                            identity.AddClaim(new Claim("schoolLogo", school?.Logo ?? ""));
                            break;
                        case "Maestro":
                            var teacher = _context.Teachers.FirstOrDefault(u =>
                                u.ApplicationUserId == userDb.Id);
                            identityUser = teacher as IIdentityUser;
                            identity.AddClaim(new Claim("email", teacher?.Email));
                            identity.AddClaim(new Claim("schoolId", teacher?.SchoolId.ToString()));
                            var school2 = _context.Schools.FirstOrDefault(x => x.Id == teacher.SchoolId);
                            identity.AddClaim(new Claim("schoolName", school2?.Name ?? ""));
                            identity.AddClaim(new Claim("schoolLogo", school2?.Logo ?? ""));
                            break;
                        case "Estudiante":
                            var student = _context.Students.FirstOrDefault(u =>
                                u.ApplicationUserId == userDb.Id);
                            identityUser = student as IIdentityUser;
                            identity.AddClaim(new Claim("email", student?.Email));
                            break;
                    }
                   
                }




                //var identity = new ClaimsIdentity(
                //   new GenericIdentity(user.UserName, "Token"),
                //   userDb.Claims.Select(x => x.ToClaim()));


                //identity.AddClaim(new Claim("id", userDb.Id.ToString()));
                identity.AddClaim(new Claim("id", identityUser?.Id.ToString()));
                identity.AddClaim(new Claim("token", user.UserName.ToString()));
                identity.AddClaim(new Claim("appId", identityUser?.ApplicationUserId));
                identity.AddClaim(new Claim("name", identityUser?.Name));
                var roles = (await _userManager.GetRolesAsync(userDb));
                foreach (var r in roles)
                {
                    identity.AddClaim(new Claim("role", r.ToString()));
                }

                return identity;

                // Credentials are invalid, or account doesn't exist

            }
            return null;
        }
    }
}
