using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Elements.Core.Interfaces;
using Elements.Core.Models;
using Elements.WebApi.Data;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Elements.WebApi.IdentityServer
{
    public class ProfileService : IProfileService
    {

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ElementsContext _context;
        private readonly ILogger _logger;

        public ProfileService(UserManager<ApplicationUser> userManager, ILoggerFactory loggerFactory, ElementsContext context)
        {
            _userManager = userManager;
            _context = context;

            _logger = loggerFactory.CreateLogger<ProfileService>();
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            string subject = context.Subject.Claims.ToList().Find(s => s.Type == "sub").Value;

            string role = null;
            List<Claim> claimList = new List<Claim>();
            IIdentityUser identityUser = null;
            try
            {
                var userDb = await _userManager.FindByIdAsync(subject);
                if (userDb != null)
                {
                    var stringRole = (await _userManager.GetRolesAsync(userDb)).FirstOrDefault()?.ToString();
                    if (stringRole != null)
                    {
                        role = stringRole;
                        switch (role)
                        {

                            case "Admin":
                            case "SuperAdmin":
                                var admin = _context.Admins.FirstOrDefault(u =>
                                    u.ApplicationUserId == userDb.Id);
                                identityUser = admin as IIdentityUser;
                                claimList.Add(new Claim("email", admin?.Email));
                                claimList.Add(new Claim("schoolId", admin?.SchoolId.ToString()));
                                var school = _context.Schools.FirstOrDefault(x => x.Id == admin.SchoolId);
                                claimList.Add(new Claim("schoolName", school?.Name ?? ""));
                                claimList.Add(new Claim("schoolLogo", school?.Logo ?? ""));
                                break;
                            case "Maestro":
                                var teacher = _context.Teachers.FirstOrDefault(u =>
                                    u.ApplicationUserId == userDb.Id);
                                identityUser = teacher as IIdentityUser;
                                claimList.Add(new Claim("email", teacher?.Email));
                                claimList.Add(new Claim("schoolId", teacher?.SchoolId.ToString()));
                                var school2 = _context.Schools.FirstOrDefault(x => x.Id == teacher.SchoolId);
                                claimList.Add(new Claim("schoolName", school2?.Name ?? ""));
                                claimList.Add(new Claim("schoolLogo", school2?.Logo ?? ""));
                                break;
                            case "Estudiante":
                                var student = _context.Students.FirstOrDefault(u =>
                                    u.ApplicationUserId == userDb.Id);
                                identityUser = student as IIdentityUser;
                                claimList.Add(new Claim("email", student?.Email));
                                claimList.Add(new Claim("firstName", student?.FirstName ?? ""));
                                claimList.Add(new Claim("lastName", student?.LastName ?? ""));
                                break;
                        }
                    }

                    if (identityUser != null)
                    {
                        //  claimList.Add (new Claim("id", userDb.Id.ToString()));
                        claimList.Add(new Claim("name", identityUser?.Name));
                        claimList.Add(new Claim("appId", identityUser.ApplicationUserId));
                        claimList.Add(new Claim("id", identityUser.Id.ToString()));
                    }

                    var roles = (await _userManager.GetRolesAsync(userDb));
                    foreach (var r in roles)
                    {
                        claimList.Add(new Claim("role", r?.ToString()));
                    }
                   
                    context.IssuedClaims = claimList;
                    return;
                }
            }
            catch
            {
                return;
            }
        }
        public Task IsActiveAsync(IsActiveContext context)
        {
            return Task.FromResult(0);
        }
    }
}
