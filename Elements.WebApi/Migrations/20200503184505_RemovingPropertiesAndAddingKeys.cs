﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Elements.WebApi.Migrations
{
    public partial class RemovingPropertiesAndAddingKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classrooms_Teachers_TeacherId",
                table: "Classrooms");

            migrationBuilder.DropTable(
                name: "StudentExercise");

            migrationBuilder.DropIndex(
                name: "IX_Stages_LessonId",
                table: "Stages");

            migrationBuilder.DropIndex(
                name: "IX_Classrooms_TeacherId",
                table: "Classrooms");

            migrationBuilder.AddColumn<decimal>(
                name: "ListeningProficiency",
                table: "StudentStage",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ReadingProficiency",
                table: "StudentStage",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "RhythmProficiency",
                table: "StudentStage",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TheoryProficiency",
                table: "StudentStage",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastVinylVideoDate",
                table: "Students",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ProExipirationDate",
                table: "Students",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Tickets",
                table: "Students",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Admins",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "BlockedStudent",
                columns: table => new
                {
                    StudentId = table.Column<int>(nullable: false),
                    SchoolId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlockedStudent", x => new { x.StudentId, x.SchoolId });
                    table.ForeignKey(
                        name: "FK_BlockedStudent_Schools_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "Schools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlockedStudent_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReceiptId = table.Column<string>(nullable: true),
                    Concept = table.Column<string>(nullable: true),
                    AmountInUSD = table.Column<decimal>(nullable: true),
                    Date = table.Column<DateTimeOffset>(nullable: false),
                    StudentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Order_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherClassroom",
                columns: table => new
                {
                    TeacherId = table.Column<int>(nullable: false),
                    ClassroomId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherClassroom", x => new { x.TeacherId, x.ClassroomId });
                    table.ForeignKey(
                        name: "FK_TeacherClassroom_Classrooms_ClassroomId",
                        column: x => x.ClassroomId,
                        principalTable: "Classrooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherClassroom_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Stages_LessonId_OrderInLesson",
                table: "Stages",
                columns: new[] { "LessonId", "OrderInLesson" });

            migrationBuilder.CreateIndex(
                name: "IX_Admins_SchoolId",
                table: "Admins",
                column: "SchoolId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BlockedStudent_SchoolId",
                table: "BlockedStudent",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_StudentId",
                table: "Order",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherClassroom_ClassroomId",
                table: "TeacherClassroom",
                column: "ClassroomId");

            migrationBuilder.AddForeignKey(
                name: "FK_Admins_Schools_SchoolId",
                table: "Admins",
                column: "SchoolId",
                principalTable: "Schools",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Admins_Schools_SchoolId",
                table: "Admins");

            migrationBuilder.DropTable(
                name: "BlockedStudent");

            migrationBuilder.DropTable(
                name: "Order");

            migrationBuilder.DropTable(
                name: "TeacherClassroom");

            migrationBuilder.DropIndex(
                name: "IX_Stages_LessonId_OrderInLesson",
                table: "Stages");

            migrationBuilder.DropIndex(
                name: "IX_Admins_SchoolId",
                table: "Admins");

            migrationBuilder.DropColumn(
                name: "ListeningProficiency",
                table: "StudentStage");

            migrationBuilder.DropColumn(
                name: "ReadingProficiency",
                table: "StudentStage");

            migrationBuilder.DropColumn(
                name: "RhythmProficiency",
                table: "StudentStage");

            migrationBuilder.DropColumn(
                name: "TheoryProficiency",
                table: "StudentStage");

            migrationBuilder.DropColumn(
                name: "LastVinylVideoDate",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "ProExipirationDate",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Tickets",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Admins");

            migrationBuilder.CreateTable(
                name: "StudentExercise",
                columns: table => new
                {
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    ExerciseId = table.Column<int>(type: "int", nullable: false),
                    Attempt = table.Column<int>(type: "int", nullable: false),
                    Date = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    Grade = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Proficiency = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Score = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentExercise", x => new { x.StudentId, x.ExerciseId, x.Attempt });
                    table.ForeignKey(
                        name: "FK_StudentExercise_Exercises_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentExercise_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Stages_LessonId",
                table: "Stages",
                column: "LessonId");

            migrationBuilder.CreateIndex(
                name: "IX_Classrooms_TeacherId",
                table: "Classrooms",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentExercise_ExerciseId",
                table: "StudentExercise",
                column: "ExerciseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classrooms_Teachers_TeacherId",
                table: "Classrooms",
                column: "TeacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
