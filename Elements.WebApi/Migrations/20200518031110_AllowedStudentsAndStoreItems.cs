﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Elements.WebApi.Migrations
{
    public partial class AllowedStudentsAndStoreItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ShopToken",
                table: "Students",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AllowedStudents",
                table: "Schools",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShopToken",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "AllowedStudents",
                table: "Schools");
        }
    }
}
