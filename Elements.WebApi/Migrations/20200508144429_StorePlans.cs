﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Elements.WebApi.Migrations
{
    public partial class StorePlans : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order_Students_StudentId",
                table: "Order");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Order",
                table: "Order");

            migrationBuilder.RenameTable(
                name: "Order",
                newName: "Orders");

            migrationBuilder.RenameIndex(
                name: "IX_Order_StudentId",
                table: "Orders",
                newName: "IX_Orders_StudentId");

            migrationBuilder.AddColumn<string>(
                name: "Language",
                table: "StudentStage",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReadingSystem",
                table: "StudentStage",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentUserId",
                table: "Students",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SubscriptionId",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Orders",
                table: "Orders",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "StoreItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Quantity = table.Column<int>(nullable: false),
                    Concept = table.Column<string>(nullable: true),
                    AmountInUSD = table.Column<decimal>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Kind = table.Column<int>(nullable: false),
                    SubscriptionId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreItems", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Students_StudentId",
                table: "Orders",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Students_StudentId",
                table: "Orders");

            migrationBuilder.DropTable(
                name: "StoreItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Orders",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Language",
                table: "StudentStage");

            migrationBuilder.DropColumn(
                name: "ReadingSystem",
                table: "StudentStage");

            migrationBuilder.DropColumn(
                name: "PaymentUserId",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "SubscriptionId",
                table: "Orders");

            migrationBuilder.RenameTable(
                name: "Orders",
                newName: "Order");

            migrationBuilder.RenameIndex(
                name: "IX_Orders_StudentId",
                table: "Order",
                newName: "IX_Order_StudentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Order",
                table: "Order",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Order_Students_StudentId",
                table: "Order",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
