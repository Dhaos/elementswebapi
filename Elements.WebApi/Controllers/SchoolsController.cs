﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using Elements.Core.Models;
using Elements.WebApi.Data;
using Elements.WebApi.Helpers;
using Elements.WebApi.Services;
using Elements.WebApi.ViewModels;
using IdentityModel.Client;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using MimeKit.Utils;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;
using CsvHelper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Elements.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Maestro,Admin,SuperAdmin")]
    public class SchoolsController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ElementsContext _context;
        private readonly EmailService _emailService;
        private readonly IWebHostEnvironment _env;

        public SchoolsController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            ElementsContext context, EmailService emailService, IWebHostEnvironment env)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _emailService = emailService;
            _env = env;
        }

        [HttpGet]
        [Authorize(Roles = "SuperAdmin")]
        [SwaggerOperation(
            Summary = "Gets all schools",
            Description = "Obtains all the schools that exist in the system. For internal use only",
            OperationId = "GetSchools"
        )]
        public ActionResult<ICollection<SchoolListViewModel>> GetSchools()
        {
            var schools = _context.Schools.AsQueryable();
            return this.CustomOk(schools);
        }
       
        [HttpPost]
        [SwaggerOperation(
            Summary = "Creates a school",
            Description = "Creates a school in the system, alongside its admin user. For internal use only",
            OperationId = "CreateSchool"
        )]
        public async Task<ActionResult> CreateSchool([FromBody] CreateSchoolViewModel model)
        {
            var school = new School
            {
                Name = model.Name,
                AllowedStudents = 100
            };
            _context.Schools.Add(school);

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.AccountEmail, Email = model.AccountEmail };
                var result = await _userManager.CreateAsync(user, model.AccountPassword);

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, UserRole.Admin.ToString());
                    Admin admin = new Admin();
                    admin.Name = model.Name;
                    admin.ApplicationUserId = user.Id;
                    admin.Email = model.AccountEmail;
                    admin.IsSuper = false;
                    _context.Admins.Add(admin);

                    school.Admin = admin;
                    await _context.SaveChangesAsync();
                    return this.CustomOk();
                }

            }
            else
            {
                return this.CustomBadRequest("Invalid username or password");
            }
            return this.CustomOk();
        }


        [HttpGet("{schoolId}")]
        [SwaggerOperation(
            Summary = "Obtains a school's details",
            Description = "Obtains the details of a school",
            OperationId = "GetSchool"
        )]
        public ActionResult<SchoolListViewModel> GetSchool(int schoolId)
        {
            var school = _context.Schools.FirstOrDefault(c => c.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Invalid school");
            var schoolVm = new SchoolListViewModel
            {
                Name = school.Name
            };

            return this.CustomOk(schoolVm);
        }
        [HttpPost("{schoolId}/allowedStudents")]
        [SwaggerOperation(
            Summary = "Sets allowed students",
            Description = "Changes the number of allowed students of the school",
            OperationId = "ChangeAllowedStudents"
        )]
        public ActionResult<SchoolListViewModel> ChangeAllowedStudents(int schoolId, [FromBody]AllowedStudentsViewModel model)
        {
            var school = _context.Schools.FirstOrDefault(c => c.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Invalid school");
            school.AllowedStudents = model.NumberOfStudents;
            _context.SaveChanges();
            return this.CustomOk();
        }

        [HttpPost("{schoolId}/toggle")]
        [SwaggerOperation(
            Summary = "Toggles a school availability",
            Description = "Activates or deactivates a school",
            OperationId = "ToggleSchool"
        )]
        public ActionResult<SchoolListViewModel> ToggleSchool(int schoolId)
        {
            var school = _context.Schools.FirstOrDefault(c => c.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Invalid school");
            school.ActiveRecord = !school.ActiveRecord;
            _context.SaveChanges();
            return this.CustomOk();
        }

        [HttpPost("{schoolId}/logo")]
        [SwaggerOperation(
            Summary = "Updates the school's logo",
            Description = "Saves the school's logo",
            OperationId = "UploadLogo"
        )]
        public ActionResult UploadLogo(int schoolId, [FromForm]AddLogoViewModel model)
        {
            var userId = this.GetUserId();
            var school = _context.Schools.FirstOrDefault(s => s.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Account not found");
            if (!school.ActiveRecord) return this.CustomBadRequest("Account invalid");
            string extension = model.Logo.FileName.Split('.').Last().ToLower();
            string path = $"\\files\\logos\\{school.Id}-{new Random().Next(0,100)}.{extension}";
            school.Logo = this.SaveFile(path, _env.WebRootPath, model.Logo);
            _context.SaveChanges();
            try
            {
                var oldLogo = school.Logo?.ToString();

                if (!string.IsNullOrWhiteSpace(oldLogo))
                {
                    this.DeleteFile(school.Logo, _env.WebRootPath);
                }
            }
            catch (Exception e)
            {
                return this.CustomOk(Domain() + school.Logo);
            }
            return this.CustomOk(Domain()+school.Logo);

        }

        [HttpGet("{schoolId}/classrooms")]
        [SwaggerOperation(
            Summary = "Obtains a school's classrooms",
            Description = "Obtains the classroom list for a specific school",
            OperationId = "GetClassrooms"
        )]
        public ActionResult<ICollection<ClassroomListViewModel>> GetClassrooms(int schoolId)
        {
            var classroom = _context.Classrooms
                .Include(c => c.Students).ThenInclude(s => s.Student)
                .Include(c => c.Teachers).ThenInclude(s => s.Teacher)
                .Where(c => c.SchoolId == schoolId);
            var classroomVm = classroom.Select(ClassroomListViewModel.FromClassroom).ToList();

            return this.CustomOk(classroomVm);
        }

        [HttpGet("{schoolId}/classrooms/{classroomId}")]
        [SwaggerOperation(
            Summary = "Obtains a classroom's details",
            Description = "Gets a specific classroom detail",
            OperationId = "GetClassroom"
        )]
        public ActionResult<ClassroomListViewModel> GetClassroom(int schoolId, int classroomId)
        {
            var classroom = _context.Classrooms
                .Include(c => c.Students).ThenInclude(s => s.Student)
                .Include(c => c.Teachers).ThenInclude(s => s.Teacher)
                .FirstOrDefault(c => c.Id == classroomId);
            var classroomVm = ClassroomListViewModel.FromClassroom(classroom);

            return this.CustomOk(classroomVm);
        }

        [HttpGet("{schoolId}/classrooms/{classroomId}/stats")]
        [SwaggerOperation(
            Summary = "Obtains a classroom's stats",
            Description = "Gets the stats of an specific classroom",
            OperationId = "GetClassroomStats"
        )]
        public ActionResult<ClassroomListViewModel> GetClassroomStats(int schoolId, int classroomId)
        {
            var classroom = _context.Classrooms
                .Include(c => c.Students).ThenInclude(s => s.Student)
                .Include(c => c.Teachers).ThenInclude(s => s.Teacher)
                .FirstOrDefault(c => c.Id == classroomId);
            var classroomVm = ClassroomListViewModel.FromClassroom(classroom);

            return this.CustomOk(classroomVm);
        }

        [HttpGet("{schoolId}/students")]
        [SwaggerOperation(
            Summary = "Obtains a school's students",
            Description = "Gets every student affiliated with at least one classroom at this school",
            OperationId = "GetStudents"
        )]
        public ActionResult<ICollection<StudentViewModel>> GetStudents(int schoolId)
        {
            var classrooms = _context.Classrooms
                .Include(c => c.Students).ThenInclude(s => s.Student)
                .Include(c => c.Teachers).ThenInclude(s => s.Teacher)
                .Where(c => c.SchoolId == schoolId).ToList().Select(ClassroomListViewModel.FromClassroom);
            var students = classrooms.Where(c => c != null && c.Students != null).SelectMany(c => c.Students).Distinct();

            return this.CustomOk(students);
        }

        [HttpGet("{schoolId}/classrooms/{classroomId}/topten")]
        [SwaggerOperation(
            Summary = "Obtains a classroom's top ten",
            Description = "Gets the top ten of a classroom's students by grade",
            OperationId = "GetClassroomTopTen"
        )]
        public ActionResult<ICollection<StudentViewModel>> GetClassroomTopTen(int schoolId, int classroomId, [FromQuery]DateTimeOffset? from = null, [FromQuery]DateTimeOffset? to = null)
        {

            var classroom = _context.Classrooms.Include(c => c.Students).ThenInclude(s => s.Student).ThenInclude(s => s.Stages).ThenInclude(s => s.Stage).FirstOrDefault(c => c.Id == classroomId);
            var students = classroom.Students.Select(x => x.Student);

            var studentStages = students.SelectMany(x => x.Stages).OrderBy(x => x.Grade).AsQueryable();
            if (from != null && to != null)
            {
                studentStages = studentStages.Where(x => x.Date.HasValue && x.Date.Value.Date >= from.Value.Date && x.Date.Value.Date <= to.Value.Date).AsQueryable();
            }

            var topTen = studentStages.Select(x => StudentViewModel.FromStudent(x.Student)).ToList();
            topTen = topTen.GroupBy(test => test.Id)
                .Select(grp => grp.First()).ToList();

            topTen = topTen.Take(10).ToList();

            return this.CustomOk(topTen);

        }

        [HttpGet("{schoolId}/stats/topten")]
        [SwaggerOperation(
            Summary = "Obtains a school's top ten",
            Description = "Gets the top ten of students by grade",
            OperationId = "GetSchoolTopTen"
        )]
        public ActionResult<ICollection<StudentViewModel>> GetSchoolTopTen(int schoolId, [FromQuery]DateTimeOffset? from = null, [FromQuery]DateTimeOffset? to = null)
        {

            var classroom = _context.Classrooms.Include(c => c.Students).ThenInclude(s => s.Student)
                .Where(x => x.SchoolId == schoolId);
            var students = classroom.SelectMany(x => x.Students.Select(xx=> xx.Student));

            var studentStages = students.SelectMany(x => x.Stages).OrderBy(x => x.Grade).AsQueryable();
            if (from != null && to != null)
            {
                studentStages = studentStages.Where(x => x.Date.HasValue && x.Date.Value.Date >= from.Value.Date && x.Date.Value.Date <= to.Value.Date).AsQueryable();
            }

            var topTen = studentStages.Select(x => StudentViewModel.FromStudent(x.Student)).ToList();
            topTen = topTen.GroupBy(test => test.Id)
                .Select(grp => grp.First()).ToList();

            topTen = topTen.Take(10).ToList();

            return this.CustomOk(topTen);


            return this.CustomOk(students);

        }

        [HttpGet("{schoolId}/students/{studentId}/stats")]
        [SwaggerOperation(
            Summary = "Obtains a student's stats",
            Description = "Gets the stats of a student",
            OperationId = "GetStudentStats"
        )]
        public ActionResult<ICollection<DetailedStatsViewModel>> GetStudentStats(int schoolId, int studentId)
        {
            var student = _context.Students
                .Include(l => l.Levels).ThenInclude(l => l.Level)
                .Include(l => l.Lessons).ThenInclude(l => l.Lesson)
                .Include(l => l.Stages).ThenInclude(l => l.Stage)
                .Include(c => c.Classrooms).ThenInclude(c => c.Classroom).FirstOrDefault(x => x.Id == studentId);

            if (student == null) return this.CustomBadRequest("Student not found");
            if (!student.Classrooms.Any(x => x.Classroom != null && x.Classroom.SchoolId == schoolId))
                return this.CustomBadRequest("Invalid permission to see student");
            var highestLesson = student.Levels.Where(x => x.Unlocked).OrderByDescending(x => x.LevelId).FirstOrDefault();
            var highestStage = student.Stages.Where(x => x.Attempt > 0 && x.Stage.LessonId == highestLesson.LevelId).OrderByDescending(x => x.StageId)
                .FirstOrDefault();
            var readingProficiency =
                student.Stages.GroupBy(x => x.StageId).Select(x => x.Max(xx => xx.ReadingProficiency)).DefaultIfEmpty().Sum(x => x ?? 0);
            var listeningProficiency =
                student.Stages.GroupBy(x => x.StageId).Select(x => x.Max(xx => xx.ListeningProficiency)).DefaultIfEmpty().Sum(x => x ?? 0);
            var theoryProficiency =
                student.Stages.GroupBy(x => x.StageId).Select(x => x.Max(xx => xx.TheoryProficiency)).DefaultIfEmpty().Sum(x => x ?? 0);
            var rhythmProficiency =
                student.Stages.GroupBy(x => x.StageId).Select(x => x.Max(xx => xx.RhythmProficiency)).DefaultIfEmpty().Sum(x => x ?? 0);
            var detailedStatsViewModel = new DetailedStatsViewModel()
            {
                Vinyls = student.Vinyls,
                HighestLesson = highestLesson.LevelId,
                HighestStage = highestStage?.Stage.OrderInLesson ?? 1,
                ReadingProficiency = readingProficiency,
                TheoryProficiency = theoryProficiency,
                RhythmProficiency = rhythmProficiency,
                ListeningProficiency = listeningProficiency
            };
            return this.CustomOk(detailedStatsViewModel);


        }

        [HttpGet("{schoolId}/students/{studentId}/stats/lessons/{lessonId}/stages")]
        [SwaggerOperation(
            Summary = "Obtains a student's stage stats",
            Description = "Gets the stats of a specific student and stage",
            OperationId = "GetStudentStats"
        )]
        public ActionResult<ICollection<StudentStageViewModel>> GetStudentLessonStats(int schoolId, int studentId, int lessonId)
        {
            var student = _context.Students
                .Include(l => l.Levels).ThenInclude(l => l.Level)
                .Include(l => l.Lessons).ThenInclude(l => l.Lesson)
                .Include(l => l.Stages).ThenInclude(l => l.Stage)
                .Include(c => c.Classrooms).ThenInclude(c => c.Classroom).FirstOrDefault(x => x.Id == studentId);

            if (student == null) return this.CustomBadRequest("Student not found");
            if (!student.Classrooms.Any(x => x.Classroom != null && x.Classroom.SchoolId == schoolId))
                return this.CustomBadRequest("Invalid permission to see student");
            var studentStages = student.Stages.Where(x => x.Attempt > 0 && x.Stage.LessonId == lessonId)
                .Select(x => StudentStageAttemptViewModel.FromStudentStage(x));

            var groupedStages = studentStages.GroupBy(x => x.StageId).Select(x => new StudentStageViewModel
            {
                StageId = x.Key,
                RelativeId = x.FirstOrDefault().RelativeId,
                TimesPlayed = x.Count(),
                Attempts = x.Select(xx => xx).ToList()
            });
            return this.CustomOk(groupedStages);
        }

        [HttpPost("{schoolId}/students/{studentId}/disassociate")]
        [SwaggerOperation(
            Summary = "Deletes the relation between a student and a school",
            Description = "Delete the student from every classroom at that school. If permanent is true, blocks the student from enrolling again.",
            OperationId = "DisassociateStudent"
        )]
        public ActionResult DisassociateStudent(int schoolId, int studentId, [FromBody]DisassociateStudentViewModel model)
        {
            var school = _context.Schools.FirstOrDefault(s => s.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Account not found");
            if (!school.ActiveRecord) return this.CustomBadRequest("Account invalid");
            var classrooms = _context.Classrooms
                .Include(c => c.Students).ThenInclude(s => s.Student).AsQueryable();
            classrooms = classrooms.Where(c => c.SchoolId == schoolId && c.Students != null && c.Students.Any(s => s.StudentId == studentId));

            foreach (var classroom in classrooms.ToList())
            {
                var studentClassroom =  classroom.Students.FirstOrDefault(s => s.StudentId == studentId);
                if (studentClassroom != null)
                {
                    _context.Entry(studentClassroom).State = EntityState.Deleted;
                }
            }

            if (model.Permanent)
            {
                var school2 = _context.Schools.Include(s => s.BlockedStudents).FirstOrDefault(s => s.Id == schoolId);
                var blockedStudent = new BlockedStudent
                {
                    StudentId = studentId
                };
                school2.BlockedStudents.Add(blockedStudent);
            }

            _context.SaveChanges();
            return this.CustomOk();
        }

        [HttpGet("{schoolId}/classrooms/{classroomId}/students")]
        [SwaggerOperation(
            Summary = "Get a classroom's students",
            Description = "Obtains the list of students that are in a specific classroom",
            OperationId = "GetClassroomStudents"
        )]
        public ActionResult<ICollection<StudentViewModel>> GetClassroomStudents(int schoolId, int classroomId)
        {
            var classroom = _context.Classrooms.Include(c => c.Students).ThenInclude(s => s.Student).FirstOrDefault(c => c.Id == classroomId);
            var students = classroom.Students.Select(s => StudentViewModel.FromStudent(s.Student));

            return this.CustomOk(students);
        }
        [HttpPost("{schoolId}/classrooms")]
        [SwaggerOperation(
            Summary = "Creates a classroom",
            Description = "Creates a new classroom for the school, with a seed teacher",
            OperationId = "CreateClassroom"
        )]
        public ActionResult CreateClassroom(int schoolId, [FromBody] CreateClassroomViewModel model)
        {
            var school = _context.Schools.FirstOrDefault(s => s.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Account not found");
            if (!school.ActiveRecord) return this.CustomBadRequest("Account invalid");
            var classroom = new Classroom()
            {
                Name = model.Name,
                SchoolId = schoolId,
                MaxStudents = model.MaxStudents,
            };
            var teacher = new TeacherClassroom
            {
                TeacherId = model.TeacherId
            };
            classroom.Teachers.Add(teacher);
            var tempCode = "";
            do
            {
                 var bytes = new byte[sizeof(Int64)];
                 RNGCryptoServiceProvider Gen = new RNGCryptoServiceProvider();
                 Gen.GetBytes(bytes);

                 long random = BitConverter.ToInt64(bytes, 0);

                 //Remove any possible negative generator numbers and shorten the generated number to 12-digits
                 tempCode = random.ToString().Replace("-", "").Substring(0, 6);
                /*string s = "";
                using (RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider())
                {
                    const string valid = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
                    while (s.Length != 6)
                    {
                        byte[] oneByte = new byte[1];
                        provider.GetBytes(oneByte);
                        char character = (char)oneByte[0];
                        if (valid.Contains(character))
                        {
                            s += character;
                        }
                    }

                    tempCode = s;
                }*/
            } while (_context.Classrooms.Any(c => c.Code == tempCode));

            classroom.Code = tempCode;
            

            _context.Classrooms.Add(classroom);
            _context.SaveChanges();

            return this.CustomOk();
        }

        [HttpGet("{schoolId}/teachers")]
        [SwaggerOperation(
            Summary = "Gets all teachers associated with a classroom",
            Description = "Creates a new classroom for the school, with a seed teacher",
            OperationId = "GetTeachers"
        )]
        public ActionResult<ICollection<TeacherViewModel>> GetTeachers(int schoolId)
        {
            /*var classrooms = _context.Classrooms.Where(c => c.SchoolId == schoolId).Include(c => c.Teachers).ThenInclude(t => t.Teacher).AsQueryable();
            var teachers = classrooms.SelectMany(c => c.Teachers.Select(t => t.Teacher)).Distinct().ToList().Select(TeacherViewModel.FromTeacher);*/
            var teachers = _context.Teachers.Where(c => c.SchoolId == schoolId).ToList().Select(TeacherViewModel.FromTeacher);
            return this.CustomOk(teachers);
        }
        
        [HttpPost("{schoolId}/teachers")]
        [SwaggerOperation(
            Summary = "Creates a new teacher",
            Description = "Creates a new teacher for this school",
            OperationId = "CreateTeacher"
        )]
        public async  Task<ActionResult> CreateTeacher(int schoolId, [FromBody] CreateTeacherViewModel model)
        {
            var school = _context.Schools.FirstOrDefault(s => s.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Account not found");
            if (!school.ActiveRecord) return this.CustomBadRequest("Account invalid");
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, UserRole.Teacher.ToString());
                    Teacher teacher = new Teacher();
                    teacher.Name = model.Name;
                    teacher.ApplicationUserId = user.Id;
                    teacher.Email = model.Email;
                    teacher.SchoolId = schoolId;
                    _context.Teachers.Add(teacher);
                    
                    await _context.SaveChangesAsync();
                    return this.CustomOk();
                }
                else
                {
                    return this.CustomBadRequest("Invalid username or password");
                }
            }
            else
            {
                return this.CustomBadRequest("Invalid infromation");
            }

        }

       [HttpPut("{schoolId}/classrooms/{classroomId}")]
        [SwaggerOperation(
            Summary = "Edits a classroom's info",
            Description = "Edits the information of a classroom",
            OperationId = "EditClassroom"
        )]
        public ActionResult EditClassroom(int schoolId, int classroomId, [FromBody] CreateClassroomViewModel model)
        {
            var school = _context.Schools.FirstOrDefault(s => s.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Account not found");
            if (!school.ActiveRecord) return this.CustomBadRequest("Account invalid");
            var classroom = _context.Classrooms.Include(x=> x.Teachers).FirstOrDefault(t => t.Id == classroomId);
            classroom.Name = string.IsNullOrWhiteSpace(model.Name) ? classroom.Name : model.Name;
            classroom.MaxStudents = model.MaxStudents > 0 ? model.MaxStudents : classroom.MaxStudents;
            if (model.TeacherId > 0 && !classroom.Teachers.Any(x => x.TeacherId == model.TeacherId))
            {
                foreach (var teacher in classroom.Teachers.ToList())
                {
                    classroom.Teachers.Remove(teacher);
                }
                classroom.Teachers.Add(new TeacherClassroom
                {
                    TeacherId = model.TeacherId
                });
            }
            _context.SaveChanges();
            return this.CustomOk();
        }
       
        [HttpPut("{schoolId}/teachers/{teacherId}")]
        [SwaggerOperation(
            Summary = "Edits a teachers's info",
            Description = "Edits the information of a teacher",
            OperationId = "EditTeacher"
        )]
        public ActionResult EditTeacher(int schoolId, int teacherId, [FromBody] CreateTeacherViewModel model)
        {
            var school = _context.Schools.FirstOrDefault(s => s.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Account not found");
            if (!school.ActiveRecord) return this.CustomBadRequest("Account invalid");
            var teacher = _context.Teachers.FirstOrDefault(t => t.Id == teacherId);
            teacher.Name = string.IsNullOrWhiteSpace(model.Name) ? teacher.Name : model.Name;
            _context.SaveChanges();
            return this.CustomOk();
        }

        [HttpDelete("{schoolId}/teachers/{teacherId}")]
        [SwaggerOperation(
            Summary = "Deletes a teacher",
            Description = "Deletes a teacher permanently",
            OperationId = "DeleteTeacher"
        )]
        public async Task<ActionResult> DeleteTeacher(int schoolId, int teacherId)
        {
            var school = _context.Schools.FirstOrDefault(s => s.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Account not found");
            if (!school.ActiveRecord) return this.CustomBadRequest("Account invalid");
            var teacher = _context.Teachers.FirstOrDefault(t => t.Id == teacherId);
            if (teacher == null) return this.CustomBadRequest("Account not found");
            var appTeacher = await _userManager.FindByIdAsync(teacher.ApplicationUserId);
            _context.Users.Remove(appTeacher);
            _context.Teachers.Remove(teacher);
            _context.SaveChanges();
            return this.CustomOk();
        }


        [HttpGet("levels")]
        [SwaggerOperation(
            Summary = "Gets all levels",
            Description = "Obtains a list of levels available",
            OperationId = "GetLevels"
        )]
        public ActionResult<ICollection<DetailedLevelViewModel>> GetLevels(int schoolId)
        {
            var levels = _context.Levels.Include(c => c.Lessons).ThenInclude(c => c.Stages).ToList();
            var levelViewModel = levels.Select(DetailedLevelViewModel.FromLevel);
            return this.CustomOk(levelViewModel);
        }

        [HttpPost("report")]
        [SwaggerOperation(
            Summary = "Generates a csv report with the average grades for selected students and lessons",
            Description = "CSV reports contains average grade, average score, and average proficiencies for the selected lessons",
            OperationId = "GenerateReport"
        )]
        public ActionResult GenerateReport(int schoolId, [FromBody] GenerateReportViewModel model)
        {
            var school = _context.Schools.FirstOrDefault(s => s.Id == schoolId);
            if (school == null) return this.CustomBadRequest("Account not found");
            if (!school.ActiveRecord) return this.CustomBadRequest("Account invalid");
            var studentIds = model.Students.Select(x => x.StudentId);
            var students = _context.Students.AsNoTracking()//.Include(s => s.Lessons).ThenInclude(l => l.Lesson)
                //.Include(s => s.Stages).ThenInclude(l => l.Stage)
                .Where(s => studentIds.Contains(s.Id) ).ToList();
            var records = new List<object>();
            var language = model.Language;
            var firstName = language == "en" ? "First Name" : "Nombre";
            var lastName = language == "en" ? "Last Name" : "Apellidos";
            var lessonString = language == "en" ? "Session " : "Sesion ";
            var gradeString = language == "en" ? "Overall Grade" : "Promedio General";
            var scoreString = language == "en" ? "Average Score" : "Puntaje General";
            var readingString = language == "en" ? "Reading Proficiency" : "Lectura";
            var theoryString = language == "en" ? "Theory Proficiency" : "Teoria";
            var rhythmString = language == "en" ? "Rhythm Proficiency" : "Ritmo";
            var listeningString = language == "en" ? "Listening Proficiency" : "Escucha";
            var allIds = model.Students.SelectMany(x => x.LessonIds).Distinct().OrderBy(x => x);
            
            foreach (var student in students)
            {
                dynamic expandoObject = new ExpandoObject();
                var expandoAsDictionary = (expandoObject as IDictionary<string, Object>);
                expandoAsDictionary.Add(firstName, student.FirstName);
                expandoAsDictionary.Add(lastName, student.LastName);
                var lessonIds = model.Students.FirstOrDefault(s => s.StudentId == student.Id).LessonIds;
                var allStagesInLessons = _context.StudentStage.Include(x => x.Stage).Where(s => s.StudentId == student.Id && lessonIds.Contains(s.Stage.LessonId)).ToList();
                var allGrades = new List<decimal>();
                foreach (var id in allIds)
                {
                    var localGrade = allStagesInLessons.Where(x => x.Stage.LessonId == id).DefaultIfEmpty()
                        .Average(x => x?.Grade ?? 0);
                    (expandoObject as IDictionary<string, Object>).Add(lessonString + id.ToString(),
                        localGrade);
                    allGrades.Add(localGrade);
                }

                var averageGrade = allGrades.DefaultIfEmpty().Average(x => x);
                var averageScore = allStagesInLessons.Where(x => x.Attempt > 0).DefaultIfEmpty().Average(x => x?.Score ?? 0);
                var averageReading = allStagesInLessons.Where(x => x.Attempt > 0).DefaultIfEmpty().Average(x => x?.ReadingProficiency ?? 0);
                var averageTheory = allStagesInLessons.Where(x => x.Attempt > 0).DefaultIfEmpty().Average(x => x?.TheoryProficiency ?? 0);
                var averageRhythm = allStagesInLessons.Where(x => x.Attempt > 0).DefaultIfEmpty().Average(x => x?.RhythmProficiency ?? 0);
                var averageListening = allStagesInLessons.Where(x => x.Attempt > 0).DefaultIfEmpty().Average(x => x?.ListeningProficiency ?? 0);

                expandoAsDictionary.Add(gradeString, averageGrade);
                expandoAsDictionary.Add(scoreString, averageScore);
                expandoAsDictionary.Add(readingString, averageReading);
                expandoAsDictionary.Add(theoryString, averageTheory);
                expandoAsDictionary.Add(rhythmString, averageRhythm);
                expandoAsDictionary.Add(listeningString, averageListening);
                //expandoObject.averageGrade = averageGrade;
                //expandoObject.averageScore = averageScore;
                //expandoObject.averageReading = averageReading;
                //expandoObject.averageTheory = averageTheory;
                //expandoObject.averageRhythm = averageRhythm;
                //expandoObject.averageListening = averageListening;

                records.Add(expandoObject);
            }
            using (var memoryStream = new MemoryStream())
            using (var streamWriter = new StreamWriter(memoryStream))
            using (var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
            {
                csvWriter.WriteRecords(records);
                streamWriter.Flush();
                memoryStream.Seek(0, SeekOrigin.Begin);
                return File(memoryStream.ToArray(), "application/octet-stream", "Report.csv");
            }

        }


        [HttpGet("videos")]
        [SwaggerOperation(
            Summary = "Gets all tutorials",
            Description = "Obtains a list of tutorials available",
            OperationId = "GetTutorials"
        )]
        public ActionResult<ICollection<Tutorial>> GetTutorials(int schoolId)
        {
            var tutorials = _context.Tutorials.ToList();
            return this.CustomOk(tutorials);
        }

        private string Domain()
        {
            return $"{Request.Scheme}://{Request.Host.ToUriComponent()}";
        }

    }

    public class AllowedStudentsViewModel
    {
        public int NumberOfStudents { get; set; }
    }

    public class StudentStageViewModel
    {
        public StudentStageViewModel()
        {
            Attempts = new List<StudentStageAttemptViewModel>();
        }
        public int StageId { get; set; }
        public int TimesPlayed { get; set; }
        public ICollection<StudentStageAttemptViewModel> Attempts { get; set; }
        public int RelativeId { get; set; }
    }

    public class StudentStageAttemptViewModel
    {
        public int StageId { get; set; }
        public int RelativeId { get; set; }
        public int Attempt { get; set; }
        public string ReadingSystem { get; set; }
        public DateTimeOffset? Date { get; set; }
        public string Language { get; set; }
        public decimal Grade { get; set; }
        public decimal? ReadingProficiency { get; set; }
        public decimal? TheoryProficiency { get; set; }
        public decimal? ListeningProficiency { get; set; }
        public decimal? RhythmProficiency { get; set; }
    public static StudentStageAttemptViewModel FromStudentStage(StudentStage s)
        {
            if (s == null) return null;
            var vm = new StudentStageAttemptViewModel
            {
                StageId = s.StageId,
                RelativeId = s.Stage?.OrderInLesson ?? 0,
                Attempt = s.Attempt,
                ReadingSystem = s.ReadingSystem,
                Language = s.Language,
                Date = s.Date,
                Grade = s.Grade,
                TheoryProficiency = s.TheoryProficiency,
                RhythmProficiency = s.RhythmProficiency,
                ReadingProficiency = s.ReadingProficiency,
                ListeningProficiency = s.ListeningProficiency
            };

            return vm;
        }
    }

    public class DetailedLevelViewModel
    {
        public DetailedLevelViewModel()
        {
            Lessons = new List<DetailedLessonViewModel>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<DetailedLessonViewModel> Lessons { get; set; }

        public static DetailedLevelViewModel FromLevel(Level l)
        {
            if (l == null) return null;
            var vm = new DetailedLevelViewModel
            {
                Id = l.Id,
                Name = l.Name
            };
            if (l.Lessons.Any())
            {
                vm.Lessons = l.Lessons.Select(DetailedLessonViewModel.FromLesson).ToList();
            }

            return vm;
        }
    }

    public class DetailedLessonViewModel
    {
        public DetailedLessonViewModel()
        {
            Stages = new List<DetailedStageViewModel>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int OrderInLevel { get; set; }
        public ICollection<DetailedStageViewModel> Stages { get; set; }

        public static DetailedLessonViewModel FromLesson(Lesson l)
        {
            if (l == null) return null;
            var vm = new DetailedLessonViewModel
            {
                Id = l.Id,
                Name = l.Name,
                OrderInLevel = l.OrderInLevel
            };
            if (l.Stages.Any())
            {
                vm.Stages = l.Stages.Select(DetailedStageViewModel.FromStage).ToList();
            }

            return vm;
        }
    }

    public class DetailedStageViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int OrderInLesson { get; set; }

        public static DetailedStageViewModel FromStage(Stage s)
        {
            if (s == null) return null;
            var vm = new DetailedStageViewModel
            {
                Id = s.Id,
                Name = s.Name,
                OrderInLesson = s.OrderInLesson
            };
            return vm;
        }
    }

    public class GenerateReportViewModel
    {
        public ICollection<GenerateStudentReportViewModel> Students { get; set; }
        public string Language { get; set; }
    }

    public class GenerateStudentReportViewModel
    {
        public int StudentId { get; set; }
        public ICollection<int> LessonIds { get; set; }
    }

    public class DisassociateStudentViewModel
    {
        public bool Permanent { get; set; }
    }

    public class StudentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public static StudentViewModel FromStudent(Student s)
        {
            if (s == null) return null;
            var vm = new StudentViewModel
            {
                Email = s.Email,
                Id = s.Id,
                Name = s.Name,
                FirstName = s.FirstName,
                LastName = s.LastName,
            };
            return vm;
        }
    }

   
    public class ClassroomListViewModel
    {
        public ClassroomListViewModel()
        {
            Teachers = new List<TeacherViewModel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string SchoolName { get; set; }
        public ICollection<TeacherViewModel> Teachers { get; set; }
        public int MaxStudents { get; set; }
        public int CurrentStudents { get; set; }
        public ICollection<StudentViewModel> Students { get; set; }

        public static ClassroomListViewModel FromClassroom(Classroom c)
        {
            var vm = new ClassroomListViewModel
            {
                Id = c.Id,
                Name = c.Name,
                Code = c.Code,
                MaxStudents = c.MaxStudents,
                SchoolName = c.School?.Name,
            };
            if (c.Students.Any())
            {
                vm.CurrentStudents = c.Students.Count;
                vm.Students = c.Students.ToList().Select(s => StudentViewModel.FromStudent(s.Student)).ToList();
            }

            if (c.Teachers.Any())
            {
                vm.Teachers = c.Teachers.Select(c => c.Teacher).ToList().Select(TeacherViewModel.FromTeacher).ToList();
            }

            return vm;
        }
    }

    public class TeacherViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public static TeacherViewModel FromTeacher(Teacher t)
        {
            if (t == null) return null;
            var vm = new TeacherViewModel
            {
                Email = t.Email,
                Id = t.Id,
                Name = t.Name
            };
            return vm;
        }
    }

    public class SchoolListViewModel
    {
        public string Name { get; set; }
    }

    public class CreateTeacherViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }

    public class CreateClassroomViewModel
    {
        public string Name { get; set; }
        public int MaxStudents { get; set; }
        public int TeacherId { get; set; }
    }

    public class CreateSchoolViewModel
    {
        public string Name { get; set; }
        public string AccountEmail { get; set; }
        public string AccountPassword { get; set; }
        
    }

    public class AddLogoViewModel
    {
        public IFormFile Logo { get; set; }
    }
}
