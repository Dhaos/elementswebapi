﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Elements.Core.Models;
using Elements.WebApi.Data;
using Elements.WebApi.Helpers;
using Elements.WebApi.Services;
using Elements.WebApi.ViewModels;
using IdentityModel.Client;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using MimeKit.Utils;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;

namespace Elements.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ElementsContext _context;
        private readonly EmailService _emailService;
        private readonly PaymentService _paymentService;

        public StoreController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            ElementsContext context, EmailService emailService, PaymentService paymentService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _emailService = emailService;
            _paymentService = paymentService;
        }

        [HttpGet]
        public async Task<ActionResult> GetItems()
        {
            var items = _context.StoreItems.ToList();
            return this.CustomOk(items);
        }
        [HttpGet("id")]
        public async Task<ActionResult> GetItems(int id)
        {
            var item = _context.StoreItems.Where(x => x.Id == id).ToList();
            return this.CustomOk(item);
        }
        [HttpPost("purchase")]
        public async Task<ActionResult> PurchaseItem([FromBody]PurchaseItemViewModel model)
        {
            //var userId = this.GetUserId();
            var token = model.Token;
            var student = _context.Students
                .Include(s => s.Levels).ThenInclude(s => s.Level)
                .Include(s => s.Lessons).ThenInclude(s => s.Lesson)
                //.Include(s => s.Stages).ThenInclude(s => s.Stage).FirstOrDefault(s => s.Id == userId);
                .Include(s => s.Stages).ThenInclude(s => s.Stage).FirstOrDefault(s => s.ShopToken == model.Token);
            if (student == null)
            {
                return this.CustomBadRequest("User not found");
            }
            var item = _context.StoreItems.First(x => x.Id == model.ItemId);
            if (string.IsNullOrWhiteSpace(model.Coupon))
            {
                if (item.AmountInUSD != item.AmountInUSD) return this.CustomBadRequest("Pricing match error");
            }

            try
            {
                switch (item.Type)
                {
                    case StoreItemType.Purchase:
                    {
                        var order = new Order
                        {
                            AmountInUSD = model.AmountInUSD,
                            Concept = item.Concept,
                            Date = DateTimeOffset.Now,
                            StudentId = student.Id,


                        };
                        var result = await _paymentService.Charge(model.CardId, model.DeviceSessionId, student, order, model.Cvv2);
                        if (result.Success)
                        {
                            order.ReceiptId = result.ExternalOrderId;
                            _context.Orders.Add(order);
                        }
                        else
                        {
                            return this.CustomBadRequest("Payment error");
                        }
                       
                        break;
                    }
                    case StoreItemType.Subscription:
                    {
                        var order = new Order
                        {
                            AmountInUSD = model.AmountInUSD,
                            Concept = item.Concept,
                            Date = DateTimeOffset.Now,
                            StudentId = student.Id,

                        };
                        var result = await _paymentService.Subscribe(model.CardId, student, order, item.SubscriptionId);
                        if (result.Success)
                        {
                            order.ReceiptId = result.ExternalOrderId;
                            order.SubscriptionId = result.ExternalId;
                            student.PaymentUserId = result.CustomerId;
                            student.ProExipirationDate = DateTimeOffset.Now.AddMonths(item.Quantity);
                            _context.Orders.Add(order);
                        }
                        else
                        {
                            return this.CustomBadRequest("Payment error");
                        } 
                        break;
                    }
                }

                student.ShopToken = null;
                _context.SaveChanges();
                return this.CustomOk();
            }
            catch (Exception e)
            {
                return this.CustomBadRequest(e.Message);
            }
          

        }

      
    }

    public class PurchaseItemViewModel
    {
        public string Token { get; set; }
        public string Cvv2 { get; set; }
        public int ItemId { get; set; }
        public string DeviceSessionId { get; set; }
        public string Coupon { get; set; }
        public decimal AmountInUSD { get; set; }
        public bool? CreateCard { get; set; }
        public string CardId { get; set; }

    }
}
