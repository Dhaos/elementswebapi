﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Elements.Core.Models;
using Elements.WebApi.Data;
using Elements.WebApi.Helpers;
using Elements.WebApi.Services;
using Elements.WebApi.ViewModels;
using IdentityModel.Client;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using MimeKit;
using MimeKit.Utils;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;

namespace Elements.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ElementsContext _context;
        private readonly EmailService _emailService;
        private readonly PaymentService _paymentService;

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            ElementsContext context, EmailService emailService, PaymentService paymentService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _emailService = emailService;
            _paymentService = paymentService;
        }

        [Route("login")]
        [HttpPost]
        [SwaggerOperation(
            Summary = "Logs in a user to the system ",
            Description = "Simple call to login. Used by any user in the system",
            OperationId = "Login"
        )]
        public async Task<ActionResult> Login([FromBody] LoginViewModel mode)
        {
            var disco = await (new HttpClient().GetDiscoveryDocumentAsync($"{Request.Scheme}://{Request.Host.ToUriComponent()}"));
            
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                Console.WriteLine(disco.Exception);
                return this.CustomBadRequest("Invalid username or password");
            }

            // request token
            /* var tokenClient = new TokenClient(discoResponse.TokenEndpoint, "mvc", "secret");

             var tokenResponse =
                 await tokenClient.RequestResourceOwnerPasswordAsync(mode.Username, mode.Password, "Elements.api");
                 */
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(disco.TokenEndpoint);
            var userFound = await _userManager.FindByNameAsync(mode.Username);
            if (userFound == null)
            {
                userFound = await _userManager.FindByEmailAsync(mode.Username);
                if (userFound != null)
                {
                    mode.Username = userFound.NormalizedUserName;
                }
            }
            var httpResponse = await httpClient.PostAsync("", new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("client_id", "mvc"),
                new KeyValuePair<string, string>("client_secret", "secret"),
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("scope", "elements.api"),
                new KeyValuePair<string, string>("username", mode.Username),
                new KeyValuePair<string, string>("password", mode.Password)
            }));

            if (!httpResponse.IsSuccessStatusCode)
            {

                return this.CustomBadRequest("Incorrect username or password");
            }

            return this.CustomOk(JObject.Parse(await httpResponse.Content.ReadAsStringAsync()));
            Console.WriteLine("\n\n");

            // call api
            var client = new HttpClient();
            //client.SetBearerToken(tokenResponse.AccessToken);

            var response = await client.GetAsync($"{Request.Scheme}://{Request.Host.ToUriComponent()}/identity");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
                return this.CustomBadRequest("Error en token");
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(content));
                return this.CustomOk(JArray.Parse(content));
            }
        }

        [Route("register")]
        [HttpPost]
        [SwaggerOperation(
            Summary = "Registers a new student via app",
            Description = "API call used to register a new student via app. The body includes the reading system and clef that will be used, an optional classroom code to enroll the student in," +
                          " and an optional 'level' attribute that will indicate the starting level, in case of previous knowledge (TBD)",
            OperationId = "Register"
        )]
        public async Task<ActionResult> Register([FromBody] AddStudentViewModel model)
        {
            /*try
            {
                //var mailResult = await _emailService.SendMail(_emailService.CreateWelcomeEmail( //model.Email//
               //     "erick.marroquin@tecnea.com.mx", model.FirstName, model.Language));
                return this.CustomOk();
            }
            catch
            {
                return this.CustomBadRequest("Mail Error");
            }*/
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser {UserName = model.Username, Email = model.Email};
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, UserRole.Student.ToString());
                    Student student = new Student();
                    student.Name = model.Name;
                    student.FirstName = model.FirstName;
                    student.LastName = model.LastName;
                    student.ApplicationUserId = user.Id;
                    student.CountryId = model.CountryId;
                    student.Clef = model.Clef;
                    student.ReadingSystem = model.ReadingSystem;
                    student.BirthDate = model.BirthDate;
                    student.Username = model.Username;
                    student.Language = model.Language;
                    student.Email = model.Email;
                    if (!string.IsNullOrWhiteSpace(model.ClassroomCode))
                    {
                        var classroom = _context.Classrooms.FirstOrDefault(cls => cls.Code == model.ClassroomCode);
                        if (classroom != null)
                        {
                            student.Classrooms.Add(new StudentClassroom
                            {
                                ClassroomId = classroom.Id
                            });
                        }
                    }

                    var levels = _context.Levels.Include(lvl => lvl.Lessons).ThenInclude(lss => lss.Stages).OrderBy(x => x.Id).AsQueryable();
                    foreach (var level in levels)
                    {
                        var studentLevel = new StudentLevel
                        {
                            LevelId = level.Id,
                        };
                        if (student.Levels.Count == 0)
                        {
                            studentLevel.Unlocked = true;
                        }

                        foreach (var lesson in level.Lessons)
                        {
                            var studentLesson = new StudentLesson
                            {
                                LessonId = lesson.Id,
                            };
                            if (student.Lessons.Count == 0)
                            {
                                studentLesson.Unlocked = true;
                            }

                            if (studentLesson.Unlocked)
                            {
                                var studentStage = new StudentStage
                                {
                                    StageId = lesson.Stages.FirstOrDefault(x => x.OrderInLesson == 1).Id,
                                    Date = DateTimeOffset.Now,
                                    Attempt = 0
                                };
                                student.Stages.Add(studentStage);
                            }
                            student.Lessons.Add(studentLesson);
                        }

                        student.Levels.Add(studentLevel);
                    }

                    _context.Students.Add(student);
                    await _context.SaveChangesAsync();
                    //await _emailService.SendMail(_emailService.CreateWelcomeEmail( /*model.Email*/"erick.marroquin@tecnea.com.mx", model.FirstName));
                    return this.CustomOk();
                }

            }
            else
            {
                return this.CustomBadRequest("Invalid username or password");
            }
            return this.CustomBadRequest("Invalid information");

        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Obtains a student's profile",
            Description = "Returns the general profile info of a student, alongside some basic stats",
            OperationId = "GetProfileInformation"
        )]
        public async Task<ActionResult<ProfileInformationViewModel>> GetProfileInformation()
        {

            var userId = this.GetUserId();
            var student = _context.Students.Include(s => s.Stages).FirstOrDefault(c => c.Id == userId);
            var profileInfo = new ProfileInformationViewModel
            {
                CountryId = student.CountryId,
                BirthDate = student.BirthDate,
                Email = student.Email,
                Username = student.Username,
                Language = student.Language,
                Name = student.Name,
                FirstName = student.FirstName,
                LastName = student.LastName,
                Country = _context.Countries.FirstOrDefault(c => c.Id == student.CountryId)?.Name,
                Vinyls = student.Vinyls,
                MaxScore = student.Stages.DefaultIfEmpty().Max(x => x.Score),
                Clef = student.Clef,
                ReadingSystem = student.ReadingSystem
            };

            if (!string.IsNullOrWhiteSpace(student.PaymentUserId))
            {
                var subscriptions = await _paymentService.GetSubscription(student);
                if (subscriptions.Any(x => x.Status == "active"))
                {
                    profileInfo.IsPro = true;
                    var activeSubscription = subscriptions.OrderByDescending(x => x.CreationDate)
                        .FirstOrDefault(x => x.Status == "active");
                    if (activeSubscription.EndDate > student.ProExipirationDate)
                    {
                        student.ProExipirationDate = activeSubscription.EndDate;
                        _context.SaveChanges();
                    }
                }
                else
                {
                    profileInfo.IsPro = student.IsPro;
                }
            }
      

            return this.CustomOk(profileInfo);

        }

        
        [Route("admin")]
        [HttpPost]
        [SwaggerOperation(
                Summary = "Creates a new user administrator",
                Description = "Sera protegido",
                OperationId = "CreateUserAdmin"
            )]
        public async Task<ActionResult> CreateUserAdmin([FromBody] AddAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, UserRole.Admin.ToString());
                    Admin admin = new Admin();
                    admin.Name = model.Name;
                    admin.Email = model.Email;
                    admin.ApplicationUserId = user.Id;
                    admin.SchoolId = model.SchoolId;
                    _context.Admins.Add(admin);
                    await _context.SaveChangesAsync();
                    return this.CustomOk();
                }
                else
                {
                    return this.CustomBadRequest("Invalid username or password");
                }
            }
            else
            {
                return this.CustomBadRequest("Invalid infromation");
            }
        }
        /*
        [Route("superAdmin")]
        [HttpPost]
        [SwaggerOperation(
            Summary = "Crea un nuevo superadmin",
            Description = "Sera protegido",
            OperationId = "CreateSuperAdmin"
        )]
        public async Task<ActionResult> CreateSuperAdmin([FromBody] AddAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, UserRole.SuperAdmin.ToString());
                    Admin admin = new Admin();
                    admin.Name = model.Name;
                    admin.Email = model.Email;
                    admin.ApplicationUserId = user.Id;
                    admin.IsSuper = true;
                    _context.Admins.Add(admin);
                    await _context.SaveChangesAsync();
                    return this.CustomOk(admin);
                }
                else
                {
                    return this.CustomBadRequest("Invalid username or password");
                }
            }
            else
            {
                return this.CustomBadRequest("Invalid infromation");
            }
        }

        */

        [Route("forgotpassword")]
        [HttpPost]
        [SwaggerOperation(
            Summary = "Sends an email to the specified user with a link to reset their password",
            Description = "Email account setup pending, sends code directly as result at the moment.",
            OperationId = "ForgotPassword"
        )]
        public async Task<ActionResult> ForgotPassword([FromBody] ForgotPasswordViewModel model)
        {
            if (model != null && !string.IsNullOrWhiteSpace(model.Email))
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    return this.CustomOk();
                }
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                code = HttpUtility.HtmlEncode(code);

                var stringRole = (await _userManager.GetRolesAsync(user)).FirstOrDefault()?.ToString();
                string role = stringRole;
                var link = this.LinkReset(Request.Scheme,
                    Request.Host.ToUriComponent(), role, code, model.Email);
                List<EmailViewModel> emails = new List<EmailViewModel>();
                var message = new MimeMessage();
                var builder = new BodyBuilder();
               /* var html = "Para generar una nuevo contraseña, por favor haz click aqui:" + link;
                builder.HtmlBody = html;
                message.Body = builder.ToMessageBody();
                message.Subject = $"Generar nueva contraseña";
                message.To.Add(new MailboxAddress(model.Email));
                message.From.Add(new MailboxAddress("no-reply@plazadentalmx.com"));

                emails.Add(new EmailViewModel
                {
                    Email = model.Email,
                    Message = message
                });

                await _emailService.SendEmail(emails);*/
                return this.CustomOk(code);
            }
            else
            {
                return this.CustomBadRequest("Invalid infromation");
            }
        }

        [Route("changepassword")]
        [HttpPost]
        [SwaggerOperation(
            Summary = "Changes a user's password",
            Description = "For a logged in user, changes their password as long as the old one matches",
            OperationId = "ChangePassword"
        )]
        public async Task<ActionResult> ChangePassword([FromBody] ChangePasswordViewModel model)
        {
            var appUserId = this.GetApplicationUserId(); 
            var user = await _userManager.FindByIdAsync(appUserId);
            if (user == null)
            {
                return this.CustomBadRequest("User not found");
            }

            var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return this.CustomOk();
            }
            else
            {
                return this.CustomBadRequest("Password change failed");
            }
       
        }

        [Route("resetpassword")]
        [HttpPost]
        [SwaggerOperation(
            Summary = "Resets a user's password",
            Description = "If given a valid code and email, changes the user's password to the one specified",
            OperationId = "ResetPassword"
        )]
        public async Task<ActionResult> ResetPassword([FromBody] ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    return this.CustomBadRequest("User not found");
                }

                var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
                if (result.Succeeded)
                {
                    return this.CustomOk();
                }
                else
                {
                    return this.CustomBadRequest("Password reset error");
                }
            }
            else
            {
                return this.CustomBadRequest("Invalid information");
            }
        }


        [HttpPost("username/verify")]
        [SwaggerOperation(
            Summary = "Validates if the username is available",
            Description = "Returns true if the user is available, false otherwise",
            OperationId = "VerifyUsernameAvailability"
        )]
        public async Task<ActionResult> VerifyUsernameAvailability([FromBody]VerifyUsernameViewModel model)
        {
            var userFound = await _userManager.FindByNameAsync(model.Username);
            return this.CustomOk(userFound != null);
        }

        [HttpPost("email/verify")]
        [SwaggerOperation(
            Summary = "Validates if the email is available",
            Description = "Returns true if the email is available, false otherwise",
            OperationId = "VerifyEmailAvailability"
        )]
        public async Task<ActionResult> VerifyEmailAvailability([FromBody]VerifyEmailViewModel model)
        {
            var userFound = await _userManager.FindByEmailAsync(model.Email);
            return this.CustomOk(userFound != null);
        }

        [HttpPost("shop/token")]
        [SwaggerOperation(
            Summary = "Generate a shop token to use web store",
            Description = "Generates a shop token that you will be able to use to purchase in the web store",
            OperationId = "GenerateShopToken"
        )]
        public async Task<ActionResult> GenerateShopToken([FromBody]StoreItemViewModel model)
        {
            var guid = Guid.NewGuid();
            var userId = this.GetUserId();
            var student = _context.Students.Include(s => s.Stages).FirstOrDefault(c => c.Id == userId);
            student.ShopToken = guid.ToString();
            _context.SaveChanges();
            var linkToStore = "";
            return this.CustomOk(linkToStore);
        }

        [Authorize]
        [HttpGet("token")]
        [SwaggerOperation(
            Summary = "Validate token",
            Description = "General API call that will be valid as long as the token sent is also valid",
            OperationId = "CheckTokenValidity"
        )]
        public ActionResult CheckTokenValidity()
        {
            return this.CustomOk();
        }

        private string Domain()
        {
            return $"{Request.Scheme}://{Request.Host.ToUriComponent()}";
        }
    }

    public class StoreItemViewModel
    {
        public int StoreItemId { get; set; }
    }

    public class VerifyUsernameViewModel
    {
        public string Username { get; set; }
    }

    public class VerifyEmailViewModel
    {
        public string Email { get; set; }
    }

    public class ChangePasswordViewModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class ProfileInformationViewModel
    {
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
        public int CountryId { get; set; }
        public string PictureUrl { get; set; }
        public DateTimeOffset BirthDate { get; set; }
        public string Language { get; set; }
        public int Vinyls { get; set; }
        public decimal MaxScore { get; set; }
        public string Clef { get; set; }
        public string ReadingSystem { get; set; }
        public bool IsPro { get; set; }
    }

    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class ResetPasswordViewModel
    {
        public string Email { get; set; }

        public string Code { get; set; }

        public string Password { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
    }

    public class AddAdminViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int SchoolId { get; set; }
    }


}

    public class AddStudentViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTimeOffset BirthDate { get; set; }
        public int CountryId { get; set; }
        public string Clef { get; set; }
        public string Language { get; set; }
        public string ReadingSystem { get; set; }
        public string ClassroomCode { get; set; }
        public int Level { get; set; } = 0;
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
