﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MimeKit;
using MimeKit.Utils;
using Elements.Core;
using Elements.Core.Interfaces;
using Elements.Core.Models;
using Elements.WebApi.Data;
using Elements.WebApi.Services;

namespace Zaza.WebApi.Controllers
{

    [Route("api/[controller]")]
    public class WebhookController : Controller
    {
        private readonly ElementsContext _context;
        private readonly IHostingEnvironment _env;
        private readonly EmailService _messageService;
        private ILogger _logger;

        public WebhookController(ElementsContext context, ILoggerFactory factory, IHostingEnvironment env, EmailService messageService)
        {
            _context = context;
            _env = env;
            _messageService = messageService;
            _logger = factory.CreateLogger("LogTest");
        }

        [HttpPost("")]
        public IActionResult RecieveWebhook([FromBody]WebhookNotification notification)
        {

            _logger.LogInformation(666, null, "Loggin Webhook. Notification={p1}, Type= {p2}, Code ={p3} ", notification, notification.Type, notification.Verification_Code);
            string externalOrderId;
            Order order;
            Student userInfo;

            switch (notification.Type)
            {
                case "verification":

                    _context.OpenPayWebhookCode.Add(new OpenPayWebhookCode { Code = notification.Verification_Code });
                    _context.SaveChanges();
                    return Ok();

                case "charge.created":
                    return Ok();
                /*TODO*/
                case "charge.succeeded":
                case "spei.received":
                    externalOrderId = (string)notification.Transaction.id;
                    order = _context.Orders.FirstOrDefault(o => o.ReceiptId == externalOrderId || o.SubscriptionId == externalOrderId);

                    if (order != null)
                    {
                        userInfo = _context.Students.FirstOrDefault(u => u.Id == order.StudentId);

                        _context.SaveChanges();

                        /*var fullOrderDb = _context.Orders
                            .FirstOrDefault(o => o.Id == order.Id);
                        if (fullOrderDb != null)
                        {
                            
                        }*/

                    }
                    return Ok();
                case "charge.refunded":
                    return Ok();
                case "payout.created":
                case "payout.succeeded":
                case "payout.failed":
                case "transfer.succeeded":
                case "fee.succeeded":
                    return Ok();
                case "chargeback.created":
                    return Ok();
                case "chargeback.rejected":
                case "chargeback.accepted":
                    return Ok();


                default:
                    return Ok();

            }
        }

       

        private MimeMessage GenerateChargeBackEmail(Student userInfo, Order order)
        {
            var message = new MimeMessage();
           /* var builder = new BodyBuilder();


            builder.TextBody = $@"Se ha generado un contracargo. Por favor revisar en OpenPay y verificar situación.\n
                                Usuario : {userInfo.Name}
                                Correo : {userInfo.Email}
                                Orden Id Interno : {order.OrderReference}
                                Orden Id Externo : {order.ExternalOrderId}";


            message.Body = builder.ToMessageBody();
            */
            return message;
        }
    }


    public class WebhookNotification
    {
        public string Type { get; set; }
        public DateTimeOffset Event_Date { get; set; }

        public dynamic Transaction { get; set; }
        public string Verification_Code { get; set; }
    }
}
