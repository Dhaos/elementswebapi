﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Elements.Core.Models;
using Elements.WebApi.Data;
using Elements.WebApi.Helpers;
using Elements.WebApi.Services;
using Elements.WebApi.ViewModels;
using IdentityModel.Client;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using MimeKit.Utils;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;

namespace Elements.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatsController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ElementsContext _context;
        private readonly EmailService _emailService;

        public StatsController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            ElementsContext context, EmailService emailService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _emailService = emailService;
        }
        [HttpPost("session/{sessionId}")]
       // [HttpPost("lessons/{lessonId}/stages/{sessionId}")]
        public ActionResult ReportStage([FromBody]StageReportViewModel model, int sessionId, int? lessonId = null)
        {
            var userId = this.GetUserId();
            var student = _context.Students
                //.Include(s => s.Levels).ThenInclude(s => s.Level)
                //.Include(s => s.Lessons).ThenInclude(s => s.Lesson)
                //.Include(s => s.Stages).ThenInclude(s => s.Stage)
                .FirstOrDefault(s => s.Id == userId);

            StudentStage studentStage;
            //if (lessonId != null)
            //{
            //    studentStage = student.Stages.FirstOrDefault(arc => arc.Stage.LessonId == lessonId && arc.Stage.OrderInLesson == sessionId && arc.Attempt != 0);
            //}
            //else
            //{
            studentStage = _context.StudentStage.OrderByDescending(x => x.Attempt).FirstOrDefault(x =>
                x.StageId == sessionId && x.Attempt != 0 && x.StudentId == student.Id);
                   // student.Stages.OrderByDescending(x => x.Attempt).FirstOrDefault(arc => arc.StageId == sessionId && arc.Attempt != 0);
            //}

            decimal grade = 0;
            decimal score = 0;
            foreach (var exercise in model.Exercises)
            {
                grade += exercise.Percentage;
                score += exercise.Score;
            }

            grade = grade / model.Exercises.Count;
            if (studentStage == null)
            {
                var actualStageId = sessionId;
                //if (lessonId != null)
                //{
                //    actualStageId =
                //        _context.Stages.FirstOrDefault(x => x.LessonId == lessonId && x.OrderInLesson == sessionId).Id;
                //}
                studentStage = new StudentStage
                {
                    StageId = actualStageId,
                    Date = DateTimeOffset.Now,
                    Attempt = 1,
                    ListeningProficiency = model.ListeningProficiency,
                    ReadingProficiency = model.ReadingProficiency,
                    RhythmProficiency = model.RhythmProficiency,
                    TheoryProficiency = model.TheoryProficiency,
                    VinylsObtained = model.VinylsObtained,
                    Grade = grade, //model.Grade,
                    Score = score, //model.Score,
                    MaxCombo = model.MaxCombo,
                    StudentId = student.Id,
                    ReadingSystem = student.ReadingSystem,
                    Language = student.Language
                };
            }
            else
            {
                var actualStageId = sessionId;
                //if (lessonId != null)
                //{
                //    actualStageId =
                //        _context.Stages.FirstOrDefault(x => x.LessonId == lessonId && x.OrderInLesson == sessionId).Id;
                //}
                studentStage = new StudentStage
                {
                    StageId = actualStageId,
                    Date = DateTimeOffset.Now,
                    Attempt = studentStage.Attempt + 1,
                    ListeningProficiency = model.ListeningProficiency,
                    ReadingProficiency = model.ReadingProficiency,
                    RhythmProficiency = model.RhythmProficiency,
                    TheoryProficiency = model.TheoryProficiency,
                    VinylsObtained = model.VinylsObtained,
                    Grade = grade, //model.Grade,
                    Score = score, //model.Score,
                    MaxCombo = model.MaxCombo,
                    StudentId = student.Id,
                    ReadingSystem = student.ReadingSystem,
                    Language = student.Language

                };
            }

            var stage = _context.Stages.FirstOrDefault(x => x.Id == sessionId);
            var vinylsSoFar = _context.StudentStage.OrderByDescending(x => x.Attempt).Where(x =>
                x.StageId == sessionId && x.Attempt != 0 && x.StudentId == student.Id).Sum(x=>x.VinylsObtained);
            if (vinylsSoFar >= stage.TotalVinyls)
            {
                
            }
            else
            {
                if (model.VinylsObtained + vinylsSoFar >= stage.TotalVinyls)
                {
                    model.VinylsObtained = stage.TotalVinyls - vinylsSoFar;
                }
               
                student.Vinyls += model.VinylsObtained;
            }

            studentStage.VinylsObtained = model.VinylsObtained;
            _context.StudentStage.Add(studentStage);
            if (studentStage.Attempt == 1)
            {
                var actualStageId = sessionId;
                //if (lessonId != null)
                //{
                //    actualStageId =
                //        _context.Stages.FirstOrDefault(x => x.LessonId == lessonId && x.OrderInLesson == sessionId).Id;
                //}
                var hasNextStage = _context.Stages.Any(x => x.Id == actualStageId + 1);
                if (hasNextStage)
                {
                    var nextStudentStage = new StudentStage
                    {
                        StageId = actualStageId + 1,
                        Date = DateTimeOffset.Now,
                        Attempt = 0,
                        StudentId = student.Id
                    };
                    _context.StudentStage.Add(nextStudentStage);
                }

                var actualStage = _context.Stages.Include(x => x.Lesson).FirstOrDefault(x => x.Id == actualStageId);
                var actualStageRelativeId = actualStage.OrderInLesson;
                var isLastInLesson = actualStageRelativeId == _context.Stages
                    .Where(x => x.LessonId == actualStage.LessonId)
                    .Max(x => x.OrderInLesson);
                if (isLastInLesson)
                {
                    var hasNextLesson = _context.Lessons.Any(x => x.Id == actualStage.LessonId + 1);
                    if (hasNextLesson)
                    {
                        var studentLesson = _context.StudentLesson.FirstOrDefault(x => x.LessonId == actualStage.LessonId + 1 && x.StudentId == student.Id);
                        if (studentLesson == null)
                        {
                            studentLesson = new StudentLesson
                            {
                                LessonId = actualStage.LessonId + 1,
                                Unlocked = true,
                                StudentId = student.Id
                            };
                            _context.StudentLesson.Add(studentLesson);
                        }
                        else
                        {
                            studentLesson.Unlocked = true;
                        }
                    }
                    var isLastInLevel = actualStage?.Lesson?.OrderInLevel == _context.Lessons
                        .Where(x => x.LevelId == actualStage.Lesson.LevelId)
                        .Max(x => x.OrderInLevel);
                    if (isLastInLevel)
                    {
                        var studentLevel = _context.StudentLevel.FirstOrDefault(x => x.LevelId == actualStage.Lesson.LevelId + 1 && student.Id == x.StudentId);
                        if (studentLevel != null)
                        {
                            studentLevel.Unlocked = true;
                        }
                    }
                }
                



            }
            /*
            foreach (var exercise in model.Exercises)
            {
                var studentExercise = student.Exercises.FirstOrDefault(exe => exe.ExerciseId == exercise.ExerciseId);
                if (studentExercise == null)
                {
                    studentExercise = new StudentExercise
                    {
                        Date = DateTimeOffset.Now,
                        Attempt = 1,
                        ExerciseId = exercise.ExerciseId,
                        Grade = exercise.Grade,
                        Score = exercise.Score
                    };
                }
                else
                {

                    studentExercise = new StudentExercise
                    {
                        Date = DateTimeOffset.Now,
                        Attempt = studentExercise.Attempt+1,
                        ExerciseId = exercise.ExerciseId,
                        Grade = exercise.Grade,
                        Score = exercise.Score
                    };
                }
                student.Exercises.Add(studentExercise);
            }*/
            
            _context.SaveChanges();

            var allStages = _context.Students.Include(c => c.Stages).Where(c => c.Stages.Any(s => s.StageId == sessionId && s.Attempt > 0))
                .SelectMany(s => s.Stages.Where(ss => ss.StageId==sessionId && ss.Attempt > 0)).OrderByDescending(x => x.Score);

            var studentsBest = allStages.ToList().GroupBy(x => x.StudentId)
                .Select(x => x.First(xx => xx.Score == x.Max(y => y.Score)));

            var topFive = studentsBest.Take(5).Select((x, index) => new LeaderboardViewModel
            {
                Score = x.Score,
                Name = _context.Students.FirstOrDefault(s => s.Id == x.StudentId).Name,
                Ranking = index+1
            });

            var ownRank = allStages
                .FirstOrDefault(x => x.StageId == sessionId && x.StudentId == userId && x.Score == studentStage.Score);
            var rank = new LeaderboardViewModel
            {
                Score = ownRank.Score,
                Name = student.Name ?? "Unavailable",
                Ranking = allStages.ToList().IndexOf(ownRank)+1
            };

            if (rank.Ranking > 5)
            {
                var allStats = topFive.ToList();
                allStats.Add(rank);
                return this.CustomOk(allStats);
            }
            return this.CustomOk(topFive);

        }

        [HttpPost("repertoire/{repertoireId}")]
        public ActionResult ReportRepertoire(int repertoireId, [FromBody]RepertoireReportViewModel model)
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(s => s.Stages).Include(s => s.Repertoires).FirstOrDefault(s => s.Id == userId);

            var studentRepertoire = student.Repertoires.FirstOrDefault(rp => rp.RepertoireId == repertoireId);
            if (studentRepertoire == null)
                /*{
                    studentRepertoire = new StudentRepertoire()
                    {
                        RepertoireId = repertoireId,
                        Date = DateTimeOffset.Now,
                        Score = model.Score,
                        TimesPlayed = 1,
                        Rating = model.Rating
                    };
                    student.Repertoires.Add(studentRepertoire);
                }*/
                return this.CustomBadRequest("Invalid song");
            else
            {
                studentRepertoire.Date = DateTimeOffset.Now;
                studentRepertoire.Score = model.Score > studentRepertoire.Score
                    ? model.Score
                    : studentRepertoire.Score;
                studentRepertoire.Rating = model.Rating > studentRepertoire.Rating ? model.Rating : studentRepertoire.Rating;
                studentRepertoire.TimesPlayed += 1;
            };

            _context.SaveChanges();
            return this.CustomOk();

        }

        [HttpPost("arcade/{arcadeGameId}")]
        public ActionResult ReportArcadeGame(int arcadeGameId, [FromBody]ArcadeGameReportViewModel model)
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(s => s.Stages).Include(s => s.ArcadeGames).FirstOrDefault(s => s.Id == userId);

            var studentArcadeGame = student.ArcadeGames.FirstOrDefault(arc => arc.ArcadeGameId == arcadeGameId);
            if (studentArcadeGame == null)
            {
                studentArcadeGame = new StudentArcadeGame()
                {
                    ArcadeGameId = arcadeGameId,
                    Date = DateTimeOffset.Now,
                    LevelAchieved = model.LevelAchieved,
                    Score = model.Score,
                    TimesPlayed = 1
                };
                student.ArcadeGames.Add(studentArcadeGame);
            }
            else
            {
                studentArcadeGame.Date = DateTimeOffset.Now;
                studentArcadeGame.LevelAchieved = model.LevelAchieved > studentArcadeGame.LevelAchieved
                    ? model.LevelAchieved
                    : studentArcadeGame.LevelAchieved;
                studentArcadeGame.Score = model.Score > studentArcadeGame.Score ? model.Score : studentArcadeGame.Score;
                studentArcadeGame.TimesPlayed += 1;
            };
          
            _context.SaveChanges();
            return this.CustomOk();

        }
    }

    public class LeaderboardViewModel
    {
        public decimal Score { get; set; }
        public string Name { get; set; }
        public int Ranking { get; set; }
    }

    public class RepertoireReportViewModel
    {
        public int Rating { get; set; }
        public decimal Ranking { get; set; }
        public decimal Score { get; set; }
    }

    public class ArcadeGameReportViewModel
    {
        public int LevelAchieved { get; set; }
        public decimal Ranking { get; set; }
        public decimal Score { get; set; }
    }

    public class StageReportViewModel
    {
        public StageReportViewModel()
        {
            Exercises = new List<ExerciseReportViewModel>();
        }

        public ICollection<ExerciseReportViewModel> Exercises { get; set; }
        public int StageId { get; set; }
        public int MaxCombo { get; set; }
        public decimal Grade { get; set; }
        public decimal Score { get; set; }
        public decimal? ListeningProficiency { get; set; }
        public int VinylsObtained { get; set; }
        public decimal? TheoryProficiency { get; set; }
        public decimal? ReadingProficiency { get; set; }
        public decimal? RhythmProficiency { get; set; }
    }

    public class ExerciseReportViewModel
    {
        public int ExerciseId { get; set; }
        public string Name { get; set; }
        public decimal Grade { get; set; }
        public decimal Percentage { get; set; }
        public decimal Duration { get; set; }
        public decimal Score { get; set; }
        public decimal MaxScore { get; set; }
        public decimal? ListeningProficiency { get; set; }
        public decimal? TheoryProficiency { get; set; }
        public decimal? ReadingProficiency { get; set; }
        public decimal? RhythmProficiency { get; set; }
    }
}
