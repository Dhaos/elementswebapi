﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Elements.Core.Models;
using Elements.WebApi.Data;
using Elements.WebApi.Helpers;
using Elements.WebApi.Services;
using Elements.WebApi.ViewModels;
using IdentityModel.Client;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using MimeKit;
using MimeKit.Utils;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;

namespace Elements.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ElementsContext _context;
        private readonly EmailService _emailService;
        private readonly IWebHostEnvironment _env;

        public StudentsController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            ElementsContext context, EmailService emailService, IWebHostEnvironment env)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _emailService = emailService;
            _env = env;
        }

        [HttpGet("profile")]
        [SwaggerOperation(
            Summary = "Gets the logged in student's profile",
            Description = "Obtains the logged in student's general profile information",
            OperationId = "GetProfile"
        )]
        public ActionResult GetProfile()
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(c => c.Classrooms).FirstOrDefault(s => s.Id == userId);
            if (student == null) return this.CustomBadRequest("Account not found");
            var profileViewModel = ProfileViewModel.FromStudent(student);

            return this.CustomOk(profileViewModel);

        }

        [HttpPost("profile")]
        [SwaggerOperation(
            Summary = "Updates the users profile",
            Description = "Updates the profile (only valid changes being name, country, language, birthdate)",
            OperationId = "UpdateProfile"
        )]
        public ActionResult UpdateProfile([FromBody]EditProfileViewModel model)
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(c => c.Classrooms).FirstOrDefault(s => s.Id == userId);
            if (student == null) return this.CustomBadRequest("Account not found");
            student.FirstName = !string.IsNullOrWhiteSpace(model.FirstName) ? model.FirstName : student.FirstName;
            student.LastName = !string.IsNullOrWhiteSpace(model.LastName) ? model.LastName : student.LastName;
            student.Language = !string.IsNullOrWhiteSpace(model.Language) ? model.Language : student.Language;
            student.BirthDate = model.BirthDate ?? student.BirthDate;
            student.CountryId = model.CountryId > 0 ? model.CountryId : student.CountryId;
            _context.SaveChanges();

            return this.CustomOk();

        }

        [HttpPost("profile/picture")]
        [SwaggerOperation(
            Summary = "Updates the users picture",
            Description = "Updates the picture",
            OperationId = "UpdatePicture"
        )]
        public ActionResult UpdatePicture([FromForm]AddPictureViewModel model)
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(c => c.Classrooms).FirstOrDefault(s => s.Id == userId);
            if (student == null) return this.CustomBadRequest("Account not found");
            string extension = model.Picture.FileName.Split('.').Last().ToLower();
            string path = $"\\files\\profile\\{student.Id}.{extension}";
            try
            {
                if (!string.IsNullOrWhiteSpace(student.PictureUrl))
                {
                    this.DeleteFile(student.PictureUrl, _env.WebRootPath);
                }
                student.PictureUrl = this.SaveFile(path, _env.WebRootPath, model.Picture);
                _context.SaveChanges();
            }
            catch
            {
                return this.CustomBadRequest("Error saving picture");
            }
            return this.CustomOk();
            
        }

        [HttpPost("profile/reading")]
        [SwaggerOperation(
            Summary = "Updates the reading system",
            Description = "Updates the reading system (letters or solfege)",
            OperationId = "UpdateReadingSystem"
        )]
        public ActionResult UpdateReadingSystem([FromBody]ReadingSystemViewModel model)
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(c => c.Classrooms).FirstOrDefault(s => s.Id == userId);
            if (student == null) return this.CustomBadRequest("Account not found");
            student.ReadingSystem = model.ReadingSystem;
            _context.SaveChanges();

            return this.CustomOk();

        }


        [HttpGet("classrooms")]
        [SwaggerOperation(
            Summary = "Gets the logged in student's classrooms",
            Description = "Obtains the associated classrooms' information, if any",
            OperationId = "GetStudentClassrooms"
        )]
        public ActionResult GetStudentClassrooms()
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(s => s.Classrooms).ThenInclude(c => c.Classroom).FirstOrDefault(s => s.Id == userId);
            if (student == null) return this.CustomBadRequest("Account not found");
            var classrooms = student.Classrooms.Select(c => ClassroomListViewModel.FromClassroom(c.Classroom));

            return this.CustomOk(classrooms);

        }
        [HttpPost("classrooms/associate")]
        [SwaggerOperation(
            Summary = "Associate with a classroom with its code",
            Description = "Associates the current user with the classroom via code, if possible",
            OperationId = "AssociateClassroom"
        )]
        public ActionResult AssociateClassroom([FromBody]ClassroomCodeViewModel model)
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(s => s.Classrooms).ThenInclude(c => c.Classroom).FirstOrDefault(s => s.Id == userId);

            if (student == null) return this.CustomBadRequest("Account not found");
            var classroom = _context.Classrooms.Include(c => c.School).ThenInclude(c => c.BlockedStudents).FirstOrDefault(c => c.Code == model.Code);
            if (classroom == null) return this.CustomBadRequest("Classroom not found");
            var studentClassroom  = student.Classrooms.FirstOrDefault(sc=> sc.ClassroomId == classroom.Id);
            if (studentClassroom != null) return this.CustomBadRequest("User already associated");
            var isBlocked = classroom.School.BlockedStudents.Any(bs => bs.StudentId == userId);
            if (isBlocked) return this.CustomBadRequest("Association failure. Please contact administrator");
            var currentSchoolClassroomsCount = _context.Schools.Include(x => x.Classrooms).ThenInclude(x => x.Students)
                .FirstOrDefault(x => x.Id == classroom.SchoolId)?.Classrooms
                .SelectMany(x => x.Students.Select(y => y.StudentId)).Distinct().Count();
            if (currentSchoolClassroomsCount + 1 >
                _context.Schools.FirstOrDefault(x => x.Id == classroom.SchoolId)?.AllowedStudents)
            {
                return this.CustomBadRequest("Association count failure. Please contact administrator");
            }

            studentClassroom = new StudentClassroom
            {
                ClassroomId = classroom.Id
            };
            student.Classrooms.Add(studentClassroom);
            _context.SaveChanges();

            return this.CustomOk();

        }

        [HttpPost("classrooms/{classroomId}/disassociate")]
        [SwaggerOperation(
            Summary = "Diassociate with a classroom",
            Description = "Disassociates the user with a  current classroom",
            OperationId = "DisassociateClassroom"
        )]
        public ActionResult DisassociateClassroom(int classroomId)
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(s => s.Classrooms).ThenInclude(c => c.Classroom).FirstOrDefault(s => s.Id == userId);

            if (student == null) return this.CustomBadRequest("Account not found");
            var classroom = _context.Classrooms.FirstOrDefault(c => c.Id == classroomId);
            if (classroom == null) return this.CustomBadRequest("Classroom not found");
            var studentClassroom = student.Classrooms.FirstOrDefault(sc => sc.ClassroomId == classroom.Id);
            if (studentClassroom == null) return this.CustomBadRequest("No association found");

            _context.Entry(studentClassroom).State = EntityState.Deleted;
            _context.SaveChanges();

            student = _context.Students.Include(s => s.Classrooms).ThenInclude(c => c.Classroom).FirstOrDefault(s => s.Id == userId);
            return this.CustomOk();

        }

        [Authorize]
        [HttpGet("levels")]
        [SwaggerOperation(
            Summary = "Gets a user's levels",
            Description = "Obtains the student's levels, including its unlock status",
            OperationId = "GetLevels"
        )]
        public ActionResult<ICollection<LevelViewModel>> GetLevels()
        {
            var userId = this.GetUserId();
            var student = _context.Students.AsNoTracking().Include(s => s.Levels).ThenInclude(l => l.Level)
                //.Include(s => s.Lessons).ThenInclude(l => l.Lesson)
                .FirstOrDefault(s => s.Id == userId);

            var levels = student.Levels.Select(LevelViewModel.FromLevel);

            return this.CustomOk(levels);

        }

        [HttpGet("levels/{levelId}/lessons")]
        [SwaggerOperation(
            Summary = "Gets a user's lessons by level",
            Description = "Obtains the lessons of a specific level",
            OperationId = "GetLessonsByLevel"
        )]
        public ActionResult<ICollection<LessonViewModel>> GetLessonsByLevel(int levelId)
        {
            var userId = this.GetUserId();
            var student = _context.Students.AsNoTracking()//.Include(s => s.Levels).ThenInclude(l => l.Level)
                //.Include(s => s.Lessons).ThenInclude(l => l.Lesson)
                //.Include(s => s.Stages).ThenInclude(l => l.Stage)
                .FirstOrDefault(s => s.Id == userId);

            //var allLessons = _context.Lessons.Where(l => l.LevelId == levelId);

            var lessons = _context.StudentLesson.AsNoTracking().Include(l => l.Lesson).Where(l => l.Lesson.LevelId == levelId && l.StudentId == student.Id).Select(LessonViewModel.FromLesson).ToList();
            var notUnlockedLessons =
                _context.Lessons.AsNoTracking().Where(l => !(lessons.Select(x => x.Id).Contains(l.Id)) && l.LevelId == levelId).ToList().Select(LessonViewModel.FromLesson).ToList();
            foreach (var lesson in lessons)
            {
                var latestStages = _context.StudentStage.AsNoTracking().Include(x => x.Stage).Where(s => s.StudentId == student.Id && lesson.Id == s.Stage.LessonId && s.Attempt > 0).DefaultIfEmpty().Max(s => s == null ? 0 : s.Stage.OrderInLesson);
                lesson.SessionsFinished = latestStages;
                //lesson.Unlocked = true;
            }

            foreach (var lesson in notUnlockedLessons)
            {
                lesson.Unlocked = false;
            }
            var groupedLessons = lessons.ToList().Union(notUnlockedLessons).OrderBy(x => x.Id).ToList();

            return this.CustomOk(groupedLessons);

        }

        [HttpGet("lessons/{lessonId}/sessions")]
        [SwaggerOperation(
            Summary = "Gets a user's sessions by lesson",
            Description = "Obtains the sessions of a specific lesson",
            OperationId = "GetSessionsByLesson"
        )]
        public ActionResult<ICollection<StageViewModel>> GetSessionsByLesson(int lessonId)
        {
            var userId = this.GetUserId();
            var student = _context.Students.AsNoTracking()//.Include(s => s.Levels).ThenInclude(l => l.Level)
                //.Include(s => s.Lessons).ThenInclude(l => l.Lesson)
                //.Include(s => s.Stages).ThenInclude(l => l.Stage)
                .FirstOrDefault(s => s.Id == userId);

            var lesson = _context.StudentLesson.FirstOrDefault(l => l.LessonId == lessonId && l.StudentId == student.Id);
            var allStages = _context.Stages.AsNoTracking().ToList();
            var stages = _context.StudentStage.AsNoTracking().Include(x => x.Stage).Where(s => lessonId == s.Stage.LessonId && s.StudentId == student.Id).Select(StageViewModel.FromStage );
            var groupedAttempts = stages.GroupBy(s => s.Id);
            var finalStages = groupedAttempts.Select(st => new StageViewModel
            {
                Grade = st.Max(x => x.Grade),
                CurrentVinyls = st.Sum(x => x.CurrentVinyls),
                TotalVinyls = allStages.FirstOrDefault(x => x.Id == st.Key).TotalVinyls,
                Id = st.Key,
                Name = allStages.FirstOrDefault(x => x.Id == st.Key).Name,
                RelativeId = allStages.FirstOrDefault(x => x.Id == st.Key).OrderInLesson,
                Unlocked = true
            });

            var otherStages = allStages.Where(x => x.LessonId == lessonId && finalStages.All(fs => fs.Id != x.Id)).Select(st => new StageViewModel
            {
                Grade = 0,
                CurrentVinyls = 0,
                TotalVinyls = st.TotalVinyls,
                Id = st.Id,
                Name = st.Name,
                RelativeId = st.OrderInLesson,
                Unlocked = false
            }).ToList();

            var completeStages = finalStages.ToList().Union(otherStages).OrderBy(x => x.Id);
            return this.CustomOk(completeStages);

        }

        [HttpGet("repertoire")]
        [SwaggerOperation(
            Summary = "Gets a user's repertoire",
            Description = "Obtains the repertoire of a user, separating the unlocked songs from the locked ones",
            OperationId = "GetRepertoire"
        )]
        public ActionResult<RepertoireCollectionViewModel> GetRepertoire()
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(s => s.Repertoires).ThenInclude(l => l.Repertoire)
                .FirstOrDefault(s => s.Id == userId);

            var ownedRepertoire = student.Repertoires.Select(x => RepertoireViewModel.FromRepertoire(x.Repertoire)).ToList();
            IQueryable<RepertoireViewModel> unownedRepertoire;
            if (ownedRepertoire.Any())
            {
                unownedRepertoire = _context.Repertoires.Where(x => !(ownedRepertoire.Select(or => or.Id).Contains(x.Id))).Select(x => RepertoireViewModel.FromRepertoire(x));
            }
            else
            {
                unownedRepertoire = _context.Repertoires.Select(x => RepertoireViewModel.FromRepertoire(x));

            }

            foreach (var repertoireViewModel in ownedRepertoire)
            {
                repertoireViewModel.Rating = student.Repertoires
                    .FirstOrDefault(x => x.RepertoireId == repertoireViewModel.Id).Rating;
                repertoireViewModel.Score = student.Repertoires
                .FirstOrDefault(x => x.RepertoireId == repertoireViewModel.Id).Score;
            }

            var repertoireCollection = new RepertoireCollectionViewModel
            {
                OwnedRepertoire = ownedRepertoire.ToList(),
                UnownedRepertoire = unownedRepertoire.ToList()
            };
            return this.CustomOk(repertoireCollection);

        }

        [HttpPost("repertoire/{repertoireId}/buy")]
        [SwaggerOperation(
            Summary = "Buys a song",
            Description = "Unlocks the song for the user with vinyls",
            OperationId = "BuyRepertoire"
        )]
        public ActionResult<RepertoireCollectionViewModel> BuyRepertoire(int repertoireId)
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(s => s.Classrooms)
                .Include(s => s.Repertoires).ThenInclude(l => l.Repertoire)
                .FirstOrDefault(s => s.Id == userId);

            var song = _context.Repertoires.FirstOrDefault(x => x.Id == repertoireId);
            if (student == null) return this.CustomBadRequest("User not found");
            if (song == null) return this.CustomBadRequest("Song not found");
            if (student.Vinyls < song.PriceInVinyls && !student.IsPro) return this.CustomBadRequest("Insufficient vinyls");
            if (student.Repertoires.Any(x => x.RepertoireId == repertoireId)) return this.CustomBadRequest("Song already bought");

            var studentRepertoire = new StudentRepertoire
            {
                RepertoireId = song.Id,
            };
            student.Repertoires.Add(studentRepertoire);
            if (!student.IsPro)
            {
                student.Vinyls -= song.PriceInVinyls;
            }
            _context.SaveChanges();

            return this.CustomOk();

        }

        [HttpGet("stats/records")]
        [SwaggerOperation(
            Summary = "Get the logged in student general records",
            Description = "Obtains the general records for the student",
            OperationId = "GetGeneralRecords"
        )]
        public ActionResult<RecordViewModel> GetGeneralRecords()
        {
            var userId = this.GetUserId();
            var student = _context.Students.Include(c => c.Levels).Include(s => s.Lessons)
                .Include(c => c.Stages)
                .FirstOrDefault(s => s.Id == userId);
            if (student == null) return this.CustomBadRequest("Account not found");
            var highestScore = student.Stages.DefaultIfEmpty().Max(x => x == null ? 0 : x.Score);
            var vinyls = student.Vinyls;
            var recordViewModel = new RecordViewModel
            {
                MaxScore = highestScore,
                Vinyls = vinyls
            };
            return this.CustomOk(recordViewModel);

        }

        [HttpGet("stats/detailed")]
        [SwaggerOperation(
            Summary = "Get the detailed user's stats",
            Description = "Obtains the detailed stats for the student",
            OperationId = "GetDetailedStats"
        )]
        public ActionResult<DetailedStatsViewModel> GetDetailedStats()
        {
            var userId = this.GetUserId();
            var student = _context.Students//.Include(c => c.Levels).ThenInclude(l => l.Level)
                //.Include(s => s.Lessons).ThenInclude(s => s.Lesson)
                //.Include(s => s.Stages).ThenInclude(s => s.Stage)
                .FirstOrDefault(s => s.Id == userId);
            if (student == null) return this.CustomBadRequest("Account not found");
            var highestLevel = _context.StudentLevel.Include(x => x.Level).Where(x => x.Unlocked && x.StudentId== student.Id).OrderByDescending(x => x.LevelId).FirstOrDefault();
            var highestLesson = _context.StudentLesson.Include(x=> x.Lesson).Where(x => x.Unlocked && x.Lesson.LevelId == highestLevel.LevelId && x.StudentId == student.Id).OrderByDescending(x => x.LessonId)
                .FirstOrDefault();
            var highestStage = _context.StudentStage.Include(x => x.Stage).Where(x => x.Attempt > 0 && x.Stage.LessonId==highestLesson.LessonId && x.StudentId == student.Id).OrderByDescending(x => x.StageId)
                .FirstOrDefault();
           
            var readingProficiency =
                student.Stages.GroupBy(x => x.StageId).Select(x => x.Max(xx => xx.ReadingProficiency)).DefaultIfEmpty().Sum(x => x ?? 0);
            var listeningProficiency =    
                student.Stages.GroupBy(x => x.StageId).Select(x => x.Max(xx => xx.ListeningProficiency)).DefaultIfEmpty().Sum(x => x ?? 0); 
            var theoryProficiency =
                student.Stages.GroupBy(x => x.StageId).Select(x => x.Max(xx => xx.TheoryProficiency)).DefaultIfEmpty().Sum(x => x ?? 0);
            var rhythmProficiency =
                student.Stages.GroupBy(x => x.StageId).Select(x => x.Max(xx => xx.RhythmProficiency)).DefaultIfEmpty().Sum(x => x ?? 0);
            var detailedStatsViewModel = new DetailedStatsViewModel()
            {
                Vinyls = student.Vinyls,
                HighestLevel = highestLevel?.LevelId ?? 1,
                HighestLevelId = highestLevel?.LevelId ?? 1,
                HighestLesson = highestLesson?.Lesson.OrderInLevel ?? 1,
                HighestLessonId = highestLesson?.LessonId ?? 1,
                HighestStage = highestStage?.Stage.OrderInLesson ?? 1,
                HighestStageId = highestStage?.StageId ?? 1,
                ReadingProficiency = readingProficiency,
                TheoryProficiency = theoryProficiency,
                RhythmProficiency = rhythmProficiency,
                ListeningProficiency = listeningProficiency
            };
            return this.CustomOk(detailedStatsViewModel);

        }

        [HttpGet("continue")]
        [SwaggerOperation(
           Summary = "Obtains highest session info to continue",
           Description = "Lists the current highest ids for the user, and the next session to continue their progress",
           OperationId = "ContinueProgress"
       )]
        public ActionResult<ContinueProgressViewModel> ContinueProgress()
        {
            var userId = this.GetUserId();
            var student = _context.Students//.Include(c => c.Levels).ThenInclude(l => l.Level)
                                           //.Include(s => s.Lessons).ThenInclude(s => s.Lesson)
                                           //.Include(s => s.Stages).ThenInclude(s => s.Stage)
                .FirstOrDefault(s => s.Id == userId);
            if (student == null) return this.CustomBadRequest("Account not found");
            var highestLevel = _context.StudentLevel.Include(x => x.Level).Where(x => x.Unlocked && x.StudentId == student.Id).OrderByDescending(x => x.LevelId).FirstOrDefault();
            var highestLesson = _context.StudentLesson.Include(x => x.Lesson).Where(x => x.Unlocked && x.Lesson.LevelId == highestLevel.LevelId && x.StudentId == student.Id).OrderByDescending(x => x.LessonId)
                .FirstOrDefault();
            var highestStage = _context.StudentStage.Include(x => x.Stage).Where(x => x.Attempt > 0 && x.Stage.LessonId == highestLesson.LessonId && x.StudentId == student.Id).OrderByDescending(x => x.StageId)
                .FirstOrDefault();
            ContinueProgressViewModel progressViewModel;
            if (highestStage != null)
            {
                var nextStage = _context.Stages.FirstOrDefault(x => highestStage.StageId + 1 == x.Id);
                if (nextStage != null)
                {
                  
                    progressViewModel = new ContinueProgressViewModel()
                    {
                        HighestLevelId = highestLevel?.LevelId ?? 1,
                        HighestLessonId = highestLesson?.LessonId ?? 1,
                        HighestStageId = highestStage?.StageId ?? 1,
                        NextStage = highestStage.StageId+1
                    };
                }
                else
                {
                    progressViewModel = new ContinueProgressViewModel()
                    {
                        HighestLevelId = highestLevel?.LevelId ?? 1,
                        HighestLessonId = highestLesson?.LessonId ?? 1,
                        HighestStageId = highestStage?.StageId ?? 1,
                        NextStage = null
                    };
                }
            }
            else
            {
                progressViewModel = new ContinueProgressViewModel()
                {
                    HighestLevelId = highestLevel?.LevelId ?? 1,
                    HighestLessonId = highestLesson?.LessonId ?? 1,
                    HighestStageId = highestStage?.StageId ?? 1,
                    NextStage = _context.Stages.FirstOrDefault(x => x.LessonId == highestLesson.LessonId)?.Id
                };
                
            }
            return this.CustomOk(progressViewModel);


        }

        [HttpPost("vinyls")]
        [SwaggerOperation(
            Summary = "Add vinyls to the user",
            Description = "Add the specified vinyls to the user. Call this method after the IAP process has completed to add the vinyls to the student",
            OperationId = "PurchaseVinyls"
        )]
        public ActionResult PurchaseVinyls([FromBody] PurchaseVinylsViewModel model)
        {
            if (model.ValidationCode == "" || true)
            {
                var userId = this.GetUserId();
                var student = _context.Students
                    .Include(s => s.Classrooms).ThenInclude(c => c.Classroom)
                    .Include(s => s.Orders)
                    .FirstOrDefault(s => s.Id == userId);

                if (student == null) return this.CustomBadRequest("Account not found");
                var order = new Order
                {
                    AmountInUSD = model.AmountInUSD,
                    Concept = "Vinyls",
                    ReceiptId = model.ReceiptId,
                };
                student.Orders.Add(order);
                student.Vinyls += model.Vinyls;
                _context.SaveChanges();
                return this.CustomOk();
            }

            return this.CustomBadRequest("Error al obtener vinyls");
        }

        [HttpPost("vinyls/video")]
        [SwaggerOperation(
            Summary = "Add vinyls to the user after watching ad video",
            Description = "Add the specified vinyls to the user. Call this method after the video has been watched to add the vinyls to the student. Does nothing if at least a day hasn't happened since the last video watched",
            OperationId = "GetVinylByVideo"
        )]
        public ActionResult GetVinylByVideo()
        {
                var userId = this.GetUserId();
                var student = _context.Students
                    .Include(s => s.Classrooms).ThenInclude(c => c.Classroom)
                    .Include(s => s.Orders)
                    .FirstOrDefault(s => s.Id == userId);

                if (student == null) return this.CustomBadRequest("Account not found");
                if (student.LastVinylVideoDate.HasValue && student.LastVinylVideoDate.Value.AddDays(1)<DateTimeOffset.Now) return this.CustomOk();
                student.Vinyls += 4;
                student.LastVinylVideoDate = DateTimeOffset.Now;
                _context.SaveChanges();
                return this.CustomOk();


        }

        [HttpPost("pro")]
        [SwaggerOperation(
            Summary = "Gives pro subscription to the user",
            Description = "Marks this user as pro for the amount of months indicated. Call this method after the IAP process has completed to give PRO access to the student",
            OperationId = "PurchasePro"
        )]
        public ActionResult PurchasePro([FromBody] PurchaseProViewModel model)
        {
            if (model.ValidationCode == "" || true)
            {
                var userId = this.GetUserId();
                var student = _context.Students.Include(s => s.Classrooms).ThenInclude(c => c.Classroom)
                    .Include(s => s.Orders)
                    .FirstOrDefault(s => s.Id == userId);

                if (student == null) return this.CustomBadRequest("Account not found");
                if (student.IsProViaSchool) return this.CustomBadRequest("User already PRO by classroom association");
                var order = new Order
                {
                    AmountInUSD = model.AmountInUSD,
                    Concept = "PRO",
                    ReceiptId = model.ReceiptId,
                };
                student.Orders.Add(order);
                student.ProExipirationDate = DateTimeOffset.Now.AddMonths(model.AmountInMonths);

                _context.SaveChanges();
                return this.CustomOk();
            }

            return this.CustomBadRequest("Error al obtener pro");
        }

        private string Domain()
        {
            return $"{Request.Scheme}://{Request.Host.ToUriComponent()}";
        }
    }

    public class ReadingSystemViewModel
    {
        public string ReadingSystem { get; set; }
    }

    public class ContinueProgressViewModel
    {
        public int HighestLevelId { get; set; }
        public int HighestLessonId { get; set; }
        public int HighestStageId { get; set; }
        public int? NextStage { get; set; }
    }

    public class RepertoireCollectionViewModel
    {
        public RepertoireCollectionViewModel()
        {
            OwnedRepertoire = new List<RepertoireViewModel>();
            UnownedRepertoire = new List<RepertoireViewModel>();
        }
        public ICollection<RepertoireViewModel> OwnedRepertoire { get; set; }
        public ICollection<RepertoireViewModel> UnownedRepertoire { get; set; }
    }

    public class RepertoireViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string Difficulty { get; set; }
        public string Link { get; set; }
        public string ProficiencyType { get; set; }
        public decimal? Score { get; set; }
        public decimal? Rating { get; set; }
        public int PriceInVinyls { get; set; }

        public static RepertoireViewModel FromRepertoire(Repertoire r)
        {
            if (r == null) return null;
            var vm = new RepertoireViewModel
            {
                Name = r.Name,
                Author = r.Author,
                Difficulty = r.Difficulty,
                Link = r.Link,
                PriceInVinyls = r.PriceInVinyls,
                Id = r.Id,
                ProficiencyType = r.ProficiencyType
            };

            return vm;
        }
    }

    public class DetailedStatsViewModel
    {
        public int HighestLesson { get; set; }
        public int HighestStage { get; set; }
        public int Vinyls { get; set; }
        public decimal ReadingProficiency { get; set; }
        public decimal TheoryProficiency { get; set; }
        public decimal ListeningProficiency { get; set; }
        public decimal RhythmProficiency { get; set; }
        public object HighestLevel { get; set; }
        public int HighestLevelId { get; set; }
        public int HighestLessonId { get; set; }
        public int? HighestStageId { get; set; }
    }

    public class ProfileViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string PictureUrl { get; set; }
        public DateTimeOffset? Birthdate { get; set; }
        public string CountryName { get; set; }
        public int CountryId { get; set; }
        public string Language { get; set; }
        public string ReadingSystem { get; set; }
        public string Clef { get; set; }
        public int Vinyls { get; set; }

        public static ProfileViewModel FromStudent(Student s)
        {
            if (s == null) return null;
            var vm = new ProfileViewModel
            {
                Email = s.Email,
                Id = s.Id,
                Name = s.Name,
                FirstName = s.FirstName,
                LastName = s.LastName,
                CountryId = s.CountryId,
                CountryName = s.Country?.Name,
                Username = s.Username,
                Language = s.Language,
                Birthdate = s.BirthDate,
                ReadingSystem = s.ReadingSystem,
                Clef = s.Clef,
                Vinyls = s.Vinyls,
                IsPro = s.IsProViaSchool || s.ProExipirationDate <= DateTimeOffset.Now
                
            };
            return vm;
        }

        public bool IsPro { get; set; }
    }

    public class AddPictureViewModel
    {
        public IFormFile Picture { get; set; }
    }


    public class EditProfileViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CountryId { get; set; }
        public string Language { get; set; }
        public DateTimeOffset? BirthDate { get; set; }

    }
    public class RecordViewModel
    {
        public decimal? MaxScore { get; set; }
        public int Vinyls { get; set; }
    }

    public class StageViewModel
    {
        public int Id { get; set; }
        public int RelativeId { get; set; }
        public bool Unlocked { get; set; }
        public string Name { get; set; }
        public decimal Grade { get; set; }
        public int CurrentVinyls { get; set; }
        public int TotalVinyls { get; set; }

        public static StageViewModel FromStage(StudentStage st)
        {
            if (st == null) return null;
            if (st.Stage == null) return null;
            var vm = new StageViewModel
            {
                RelativeId = st.Stage.OrderInLesson,
                Name = st.Stage.Name,
                Id = st.StageId,
                Unlocked = st.Attempt > 0 || st.Stage.OrderInLesson == 1,
                CurrentVinyls = st.VinylsObtained,
                Grade = st.Grade
            };
            return vm;
        }
    }

    public class LessonViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SessionsFinished { get; set; }
        public bool Unlocked { get; set; }

        public static LessonViewModel FromLesson(StudentLesson sl)
        {
            if (sl == null) return null;
            if (sl.Lesson == null) return null;
            var vm = new LessonViewModel
            {
                Id = sl.LessonId,
                Name = sl.Lesson.Name,
                Unlocked = sl.Unlocked
            };
            return vm;
        }

        public static LessonViewModel FromLesson(Lesson sl)
        {
            if (sl == null) return null;
            var vm = new LessonViewModel
            {
                Id = sl.Id,
                Name = sl.Name
            };
            return vm;
        }
    }

    public class LevelViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Unlocked { get; set; }

        public static LevelViewModel FromLevel(StudentLevel sl)
        {
            if (sl == null) return null;
            var vm = new LevelViewModel
            {
                Id = sl.LevelId,
                Name = sl.Level?.Name,
                Unlocked = sl.Unlocked
            };

            return vm;
        }
    }

    public class PurchaseProViewModel
    {
        public int AmountInMonths { get; set; }
        public decimal AmountInUSD { get; set; }
        public string ValidationCode { get; set; }
        public string ReceiptId { get; set; }

    }

    public class PurchaseVinylsViewModel
    {
        public int Vinyls { get; set; }
        public string ValidationCode { get; set; }
        public decimal AmountInUSD { get; set; }

        public string ReceiptId { get; set; }
    }

    public class ClassroomCodeViewModel
    {
        public string Code { get; set; }
    }
}
